\version "2.16.1"

\include "MNP-scripts.ly"

theMusic = \relative c' { 
  \time 4/4
  c cis d dis e f fis g gis a ais b c d e f g a b c 
  \time 2/4
  \clef treble 
  <c,, e g>4
  \clef bass
  % \set Staff.middleCPosition = #18
  <c e g>4
  \clef treble 
  <c e g>4
  % \clef alto
  % \set Staff.middleCPosition = #6
  % <c e g>4
}


  \new Staff { \numericTimeSignature \theMusic }
  \new StaffFiveLineOne { \theMusic }
  \new StaffFiveLineTwo { \theMusic }
  \new StaffParncuttOne { \theMusic }
  \new StaffParncuttTwo { \theMusic }
  \new StaffIsomorphOne { \theMusic }
  \new StaffIsomorphTwo { \theMusic }
  \new StaffBeyreutherChromaticSixSix { \theMusic }
  \new StaffBeyreutherUntitledOne { \theMusic }
  \new StaffBeyreutherUntitledTwo { \theMusic }
  \new StaffChromaticLyre { \theMusic }
  \new StaffTwinlineReed { \theMusic }
  \new StaffTwinlineBlackTriangle { \theMusic }
  \new StaffTwinlineBlackOval { \theMusic }
  \new StaffTwinNote { \theMusic }
  \new StaffMutoOne { \theMusic }
  \new StaffMutoTwo { \theMusic }
  \new StaffKlavarMirckOne { \theMusic }
  \new StaffKlavarMirckTwo { \theMusic }
  \new StaffNumberedNotes { \theMusic }
  % \new StaffTester { \theMusic }