title    = "3 Octaves Chromatic E to E"
subtitle = "Example of SLING music notation"

\version "2.18.2"
\language "english"
\include "msg.ly"

\paper {
  markup-system-spacing.basic-distance = 9
  system-system-spacing.padding = 6
  top-margin = 15
  bottom-margin = 15
  indent = 0
}

melody =
{
 \clef bass \tn-clef "treble_8"
 \key c \major
 \time 6/4
 \tempo 4 = 120

 \staff-sling
 e,8 f, gf, g, af, a, bf, b, c df d ef | % 1
 e8 f gf g af a bf b c' df' d' ef' | % 2
 e'8 f' gf' g' af' a' bf' b' c'' df'' d'' ef'' | % 3
 e''1 | %4
}


\score {
<<
    \new Bracket
    \new Staff
    \melody
>>
  \layout { }
  \midi { }
}
