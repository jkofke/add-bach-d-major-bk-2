\version "2.12.0"
\header{
  texidoc="@code{staffLineLayoutFunction} is used to change the position of the notes.
This sets @code{staffLineLayoutFunction} to @code{ly:pitch-semitones} to
produce a chromatic scale with the distance between a consecutive
space and line equal to one semitone.
"
}

%{ 
Comment: This file demonstrates the use of the following to achieve various chromatic staff notation systems:

     staffLayoutFunction = #ly:pitch-semitones 
          (customizes the note placement on the staff to chromatic staff layout, semitone spacing)

     staffLineLayoutFunction = #(lambda (p) (floor (/ (ly:pitch-semitones p) 2)))
          (customizes the note placement on the staff to wholetone spacing, 2 notes at each staff position.  
           The number 2 corresponds to the number of notes placed at a given staff position,
           so it can be customized to other values.)
     
     middleCPosition = #-6
          (changes the vertical position of middle C and all other notes in turn)

     clefPosition    
          (changes the vertical position of the clef)

     \override Staff.StaffSymbol #'line-positions = #'( 4 2 0 -2 -4 )    
          (customizes the location of staff lines.  4 2 0 -2 -4 are the positions for the traditional staff)

  	 \remove "Accidental_engraver"   
          (Prevents accidental signs from being drawn)

     \remove "Key_engraver"          
          (Prevents key signature from being drawn)
%}



scales = \relative c' {
  c cis d dis e f fis g gis
  a ais b c c,2 d e f g a b c
}


%{ Generic-5-line chromatic staff  %}

\new Staff \with {
  \remove "Accidental_engraver"
  \remove "Key_engraver" 
  staffLineLayoutFunction = #ly:pitch-semitones
  middleCPosition = #-6
  clefGlyph = #"clefs.G"
  clefPosition = #(+ -6 7)
}
{
  \override Staff.StaffSymbol #'line-positions = #'( 4 2 0 -2 -4 )
  \time 4/4
  <<
    \scales
    \context NoteNames {
      \set printOctaveNames= ##f
      \scales
    }
  >>
}


%{ Parncutt-Tetragram 4-line chromatic staff  %}

\new Staff \with {
  \remove "Accidental_engraver"
  \remove "Key_engraver" 
  staffLineLayoutFunction = #ly:pitch-semitones
  middleCPosition = #-7
  clefGlyph = #"clefs.G"
  clefPosition = #(+ -7 7)
}
{
  \override Staff.StaffSymbol #'line-positions = #'( 4 2 0 -2 )
  \time 4/4
  <<
    \scales
    \context NoteNames {
      \set printOctaveNames= ##f
      \scales
    }
  >>
}


%{ Minor-3rds line-spacing chromatic staff  %}

\new Staff \with {
  \remove "Accidental_engraver"
  \remove "Key_engraver" 
  staffLineLayoutFunction = #ly:pitch-semitones
  middleCPosition = #-6
  clefGlyph = #"clefs.G"
  clefPosition = #(+ -6 7)
}
{
  \override Staff.StaffSymbol #'line-positions = #'(   3 -0 -3 )
  \time 4/4
  <<
    \scales
    \context NoteNames {
      \set printOctaveNames= ##f
      \scales
    }
  >>
}


%{ Major-3rds line-spacing chromatic staff  %}

\new Staff \with {
  \remove "Accidental_engraver"
  \remove "Key_engraver" 
  staffLineLayoutFunction = #ly:pitch-semitones
  middleCPosition = #-10
  clefGlyph = #"clefs.G"
  clefPosition = #(+ -10 7)
}
{
  \override Staff.StaffSymbol #'line-positions = #'(10 6   -2 -6 )
  \time 4/4
  <<
    \scales
    \context NoteNames {
      \set printOctaveNames= ##f
      \scales
    }
  >>
}


%{ Tritone line-spacing staff  %}
\new Staff \with {
  \remove "Accidental_engraver"
  \remove "Key_engraver" 
  staffLineLayoutFunction = #ly:pitch-semitones
  middleCPosition = #-8
  clefGlyph = #"clefs.G"
  clefPosition = #(+ -8 7)
}
{
  \override Staff.StaffSymbol #'line-positions = #'( 6 0  -6)
  \time 4/4
  <<
    \scales
    \context NoteNames {
      \set printOctaveNames= ##f
      \scales
    }
  >>
}

%{ Klavar-style 7-5 line-spacing staff  %}
\new Staff \with {
  \remove "Accidental_engraver"
  \remove "Key_engraver" 
  staffLineLayoutFunction = #ly:pitch-semitones
  middleCPosition = #-6
  clefGlyph = #"clefs.G"
  clefPosition = #(+ -6 7)
}
{
  \override Staff.StaffSymbol #'line-positions = #'( 4 2 0 -3 -5 )
  \time 4/4
  <<
    \scales
    \context NoteNames {
      \set printOctaveNames= ##f
      \scales
    }
  >>
}

%{ TwinNote style staff, wholetone spacing between staff positions
Note the special scheme function used for staffLineLayoutFunction  %}
\new Staff \with {
  \remove "Accidental_engraver"
  \remove "Key_engraver" 
  staffLineLayoutFunction = #(lambda (p) (floor (/ (ly:pitch-semitones p) 2)))
  middleCPosition = #-6
  clefGlyph = #"clefs.G"
  clefPosition = #(+ -6 7)
}
{
  \override Staff.StaffSymbol #'line-positions = #'( 4 2  -2 -4 )
  \time 4/4
  <<
    \scales
    \context NoteNames {
      \set printOctaveNames= ##f
      \scales
    }
  >>
}

