---
import MnpLayout from "../../../layouts/MnpLayout.astro";
import { makeMetaTitle } from "../../../utils/utils";
---

<MnpLayout
  metaTitle={makeMetaTitle("Open Source Strategy", "Software")}
  metaDescription="Using open-source software with alternative music notation systems. A strategy for how this could be done for maximum benefit with minimal effort."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="page type-page status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">Open Source Strategy</h1>
        </header>

        <div class="entry-content">
          <h3>Towards Software for Alternative Music Notations</h3>
          <p>
            What follows is a strategy for creating a basic &#8220;transnotator
            application&#8221; for alternative music notations with minimal
            effort and expense while also providing an optimum set of features
            and benefits. The aim is to allow for automated transnotation
            (translation) of music from traditional notation into various
            alternative notation systems. This approach was outlined in a paper
            presented at the 2003 <a href="/mnma/">MNMA</a> conference by Paul Morris,
            and what follows is based on that paper. See <a href="/software/">
              Software</a
            > for more about our work in this area.<a id="Recent"></a>
          </p>
          <p>&nbsp;</p>
          <h2 class="divider-heading">
            Recent Developments in Music Notation Software
          </h2>
          <p>
            Relatively recent developments in music notation software have made
            it much more feasible to create a transnotation application that is
            accessible and useful to a wide number of people.
          </p>
          <h3>MusicXML</h3>
          <p>
            There has finally emerged a successful standard interchange file
            format for music notation called <a href="http://www.musicxml.org"
              >MusicXML</a
            >. MusicXML files can be read or written by a large number of music
            applications that run on a wide variety of computer platforms,
            including the two most popular commercial music notation
            applications, Finale and Sibelius.
          </p>
          <h3>Open-Source Applications</h3>
          <p>
            There are now several open-source music notation applications that
            are free to download and use (for example: <a
              href="http://lilypond.org"
              target="_blank">LilyPond</a
            >, <a
              title="Kevin Dalley’s LilyPond Code"
              href="http://sourceforge.net/apps/mediawiki/canorus/"
              target="_blank">Canorus</a
            >, <a href="http://www.musescore.org/" target="_blank">MuseScore</a
            >). Because they are open-source, their source code is also freely
            accessible and could be adapted and expanded to visually render
            music in alternative notations as well as traditional notation. By
            beginning with existing software rather than starting from scratch,
            a great deal of programming effort could be saved.
          </p>
          <h3>The Internet and Online Music Libraries</h3>
          <p>
            The internet is an excellent distribution channel, providing people
            all over the world with the means to download and use such a
            transnotation application. It also provides access to growing
            libraries of sheet music that is freely available for download in
            electronic file formats such as MusicXML, for example: <a
              href="http://www.mutopiaproject.org/"
              target="_blank">Mutopia Project</a
            >, <a
              href="http://www.gutenberg.org/wiki/Gutenberg:The_Sheet_Music_Project"
              target="_blank">Gutenberg Sheet Music Project</a
            >, <a href="http://www.wikifonia.org/">Wikifonia</a>, <a
              href="http://imslp.org"
              target="_blank"
              ><b>International Music Score Library Project (IMSLP)</b>
            </a><a id="Note"></a>.
          </p>
          <p>&nbsp;</p>
          <h2 class="divider-heading">
            Transnotation and Music Notation File Formats
          </h2>
          <p>
            Since the actual music stored in a computer notation file is the
            same whether it is displayed in traditional notation or an
            alternative notation (an F# is an F# in any notation), there is no
            need for a separate file, or file format for alternative notations.
            Transnotation does not require changing the content of the file, but
            only how that content is displayed. Transnotation turns out to be a
            task of re-presentation. So what is needed is an application that
            merely displays existing traditional music notation files (such as
            MusicXML files) in alternative notation systems. <a id="MusicXML"
            ></a>
          </p>
          <p>
            <img
              loading="lazy"
              class="alignnone"
              alt="Music File Format Diagram"
              src="/wp-content/uploads/2013/03/onefileformat.gif"
              width="200"
              height="400"
            />
          </p>
          <h2 class="divider-heading">MusicXML</h2>
          <p>
            Soon after its introduction in the spring of 2002, MusicXML became
            by far the most successful and widely adopted interchange file
            format for music notation. See the <a
              href="http://www.musicxml.com"
              target="_blank">MusicXML web site</a
            > for an up to date list of all the applications and file formats that
            work with MusicXML. Building a transnotation application that works with
            this standard file format would ensure that it would be usable by the
            most people. It would also be effectively leveraging the functionality
            (editing, scanning, etc.) of other software for use with alternative
            notations. This would open up many sources of music for transnotation
            into alternative notations.
          </p>
          <p>
            &nbsp;<br />
            <img
              loading="lazy"
              alt="Software Conversion Flow Diagram"
              src="/wp-content/uploads/2013/03/software-conversion-flow.gif"
              width="560"
              height="440"
            />
          </p>
          <p><a id="Open"></a></p>
          <h2 class="divider-heading">
            Open Source Music Notation Viewer Applications
          </h2>
          <p>
            Displaying music in all of its complex variations on a computer is
            not a trivial task. This is why it makes sense to adapt and extend
            an existing open-source software application where much of the work
            has already been done rather than starting from scratch. The
            majority of such an application could be re-used, more or less
            as-is, including:
          </p>
          <ul>
            <li>The user interface.</li>
            <li>The part of the application that reads files (the parser).</li>
            <li>
              Most of the output code, including code for proper spacing between
              various elements on the staff. This includes on-screen output,
              output to printers, and output to graphical file formats such as
              PDF or Postscript.
            </li>
          </ul>
          <p>
            By modifying part of the output code so that it displays music in
            alternative notations it would be possible to leverage the rest of
            the functionality that already exists in the application. Of course,
            there is a good deal of effort involved in learning how an existing
            application works in order to be able to modify it, but given the
            amount of effort that would be saved, this still seems to be a
            preferable strategy. One way around this difficulty would be to work
            with or hire the programmers who developed the application and know
            it inside-out.<a id="GUIDO"></a>
          </p>
          <p>&nbsp;</p>
          <h2 class="divider-heading">LilyPond</h2>
          <p>
            Currently, <a href="http://www.lilypond.org">LilyPond</a> is probably
            the most promising open-source application that could be adapted to display
            and print music in alternative music notation systems. Some work toward
            adding this functionality has already begun. For more information see
            <a href="/software/lilypond/"
              >LilyPond and Alternative Music Notations</a
            >.
          </p>
          <p>
            LilyPond can now import MusicXML files, and the <a
              href="http://www.mutopiaproject.org/"
              target="_blank">Mutopia Project</a
            > offers an online library of music freely available in its native LilyPond
            file format. Importing MusicXML files adds a trivial step to the transnotation
            process.
          </p>
          <p>
            Another open-source music notation viewer application is the <a
              href="http://www.noteserver.org/noteserver.html"
              target="_blank">GUIDO Noteserver</a
            >, but it is much less actively developed than LilyPond.
          </p>
          <p>&nbsp;</p>
          <h2 class="divider-heading">Links to Related Sites</h2>
          <p><a href="http://lilypond.org" target="_blank">LilyPond</a></p>
          <p>Applications that work with, or export to LilyPond:</p>
          <ul>
            <li>
              <a href="http://www.musescore.org/" target="_blank">MuseScore</a> (Windows,
              Mac, Linux)
            </li>
            <li>
              <a
                href="http://sourceforge.net/apps/mediawiki/canorus/"
                target="_blank">Canorus</a
              > (Windows, Mac, Linux, successor to NoteEdit)
            </li>
            <li>
              <a href="http://www.denemo.org/" target="_blank">Denemo</a> (Windows,
              Linux)
            </li>
            <li>
              <a href="http://www.rosegardenmusic.com" target="_blank"
                >Rosegarden</a
              > (Linux)
            </li>
            <li>
              <a
                href="http://vsr.informatik.tu-chemnitz.de/~jan/nted/nted.xhtml"
                target="_blank">NtEd</a
              > (Linux)
            </li>
          </ul>
          <p>&nbsp;</p>
          <p><a href="http://www.musicxml.com" target="_blank">MusicXML</a></p>
          <p>
            <a href="http://www.w3.org/XML/" target="_blank">XML</a><br />
            <a href="http://www.opensource.org" target="_blank">Open-Source</a>
          </p>
          <p>
            <a href="http://www.unicode.org" target="_blank">Unicode</a><br />
            <a
              href="http://www.unicode.org/charts/PDF/U1D100.pdf"
              target="_blank">Chart showing unicode music symbols</a
            >
          </p>
          <p>
            GUIDO: <a href="http://www.salieri.org/" target="_blank"
              >GUIDO File Format</a
            >, <a href="http://www.noteserver.org/" target="_blank"
              >GUIDO NoteServer &amp; NoteViewer</a
            >, <a href="http://guidolib.sourceforge.net/" target="_blank"
              >GUIDOLib<br />
            </a>MusicXML to GUIDO converters: <a
              href="http://libmusicxml.sourceforge.net/"
              target="_blank">MusicXML Library</a
            >, pyScore<br />
            Other applications that work with GUIDO: <a
              href="http://debussy.music.ubc.ca/NoteAbility/index.html"
              target="_blank">NoteAbility Pro</a
            >, NoteAbility Lite, <a
              href="http://debussy.music.ubc.ca/NoteWriter/index.html"
              target="_blank">NoteWriter</a
            >
          </p>
          <p>
            An exhaustive index of music notation file formats, among other
            things music-notational, maintained by Gerd Castan: <a
              href="http://www.music-notation.info"
              target="_blank"
              >http://www.music-notation.info
            </a>
          </p>
          <p>
            Stuart Cunningham’s MS thesis “Music File Formats and Project XEMO”
            An excellent discussion of Project Xemo, MusicXML, NIFF etc.
          </p>
        </div>
      </article>
    </div>
  </div>
</MnpLayout>
