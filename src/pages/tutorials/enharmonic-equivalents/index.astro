---
import MnpLayout from "../../../layouts/MnpLayout.astro";
import { makeMetaTitle } from "../../../utils/utils";
---

<MnpLayout
  metaTitle={makeMetaTitle("Enharmonic Equivalents", "Tutorials")}
  metaDescription="Consider the representation of enharmonic equivalents in traditional notation and on a chromatic staff, and the reasons for differentiating between them."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="tutorials type-tutorials status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">Enharmonic Equivalents</h1>
        </header>

        <div class="entry-content">
          <p>
            Some musicians may object to alternative music notation systems that
            use a <a href="/">chromatic staff</a> because of their (presumed) omission
            of the traditional distinction between enharmonically equivalent notes
            (like C# and Db). This tutorial looks at the reasoning behind this objection,
            considers several ways it can be addressed, and ultimately shows it to
            be unfounded. This is an obscure and often complex topic, and some background
            knowledge about <a href="http://en.wikipedia.org/wiki/Enharmonic"
              >enharmonic equivalents</a
            >, <a href="http://en.wikipedia.org/wiki/Musical_tuning"
              >tuning systems and practice</a
            >, and <a href="http://en.wikipedia.org/wiki/Diatonic_functionality"
              >diatonic function</a
            > may be helpful.
          </p>
          <p>&nbsp;</p>
          <h2 class="divider-heading">
            Enharmonic Equivalents in Traditional Notation and on Chromatic
            Staves
          </h2>
          <p>
            In western music theory and practice, notes such as C# and Db are
            understood to be &#8220;enharmonically equivalent.&#8221; If you
            include double sharps and double flats, there are three of these
            notes for all but one of the twelve degrees of the chromatic scale:
          </p>
          <p>
            <img
              loading="lazy"
              alt="Enharmonically equivalent spellings for the 12 degrees of the chromatic scale"
              src="/wp-content/uploads/2013/03/enharmonicNames5.png"
              width="649"
              height="150"
            />
          </p>
          <p>These enharmonically equivalent notes:</p>
          <ul>
            <li>have <em>different names</em> and</li>
            <li>
              are <em>represented differently</em> in traditional notation
            </li>
          </ul>
          <p>And yet they:</p>
          <ul>
            <li>
              <em>sound the same</em> since they have the <em>same pitch</em>(or
              very similar pitches, see below) and
            </li>
            <li>are <em>played the same way</em> on most instruments</li>
          </ul>
          <p>
            This is another example of inconsistency in traditional
            notation&#8217;s representation of pitch. Notes that sound the same
            may appear on different lines or spaces. The traditional diatonic
            staff already has an irregular pitch axis, and the use of accidental
            signs makes the relationship between pitches less consistent and
            more obscure. (See our tutorial on <a href="/tutorials/intervals/"
              >Intervals in Traditional Music Notation</a
            >.)
          </p>
          <p>&nbsp;</p>

          <h3>Enharmonic Equivalents in Traditional Notation</h3>
          <p>
            Notes that sound the same and are played the same appear at
            different vertical positions on the staff.
          </p>
          <p>
            With only sharps, flats, and naturals: (7 diatonic notes) x (3
            variants) = 21 notes per octave:
          </p>
          <p>
            <img
              alt="Illustration of enharmonic equivalents in traditional notation"
              src="/wp-content/uploads/2012/07/enharmonics-traditional.png"
            />
          </p>
          <p>
            And again including double sharps and double flats: (7 diatonic
            notes) x (5 variants) = 35 notes per octave:
          </p>
          <p>
            <img
              alt="Illustration of enharmonic equivalents in traditional notation including double sharps and flats"
              src="/wp-content/uploads/2012/07/enharmonics-traditional-double-sharps-flats.png"
            />
          </p>
          <p>
            Chromatic staves are more consistent than the traditional diatonic
            staff in representing the relationships between pitches. Notes that
            are enharmonically equivalent share the same position on a chromatic
            staff. Notes that are played the same, and sound the same, also look
            the same.
          </p>
          <p>
            Below is an illustration of what these notes might look like on a
            basic five-line chromatic staff. (We are using this five-line
            chromatic staff just to illustrate the principle, not to suggest
            that it is preferable to other types of chromatic staff.)
          </p>
          <h3>Enharmonic Equivalents on a Generic 5-Line Chromatic Staff</h3>
          <p>
            Notes that generally sound the same and are played the same share
            the same vertical position on the staff. (Shown without any
            alternative accidental signs, see below.)
          </p>
          <p>
            With only sharps, flats, and naturals: (7 diatonic notes) x (3
            variants) = 21 notes per octave:
          </p>
          <p>
            <img
              alt="Illustration of enharmonic equivalents on a 5-line chromatic staff"
              src="/wp-content/uploads/2012/07/enharmonics-chromatic-staff.png"
            />
          </p>
          <p>
            And again including double sharps and double flats: (7 diatonic
            notes) x (5 variants) = 35 notes per octave:
          </p>
          <p>
            <img
              alt="Illustration of enharmonic equivalents on a 5-line chromatic staff including double sharps and flats"
              src="/wp-content/uploads/2012/07/enharmonics-chromatic-staff-double-sharps-flats.png"
            />
          </p>
          <p>
            This raises a series of questions that we address in the remainder
            of this tutorial:
          </p>
          <ul>
            <li>
              Is there a need to differentiate between enharmonic equivalents?
              What are the reasons for doing so? How compelling are they?
            </li>
            <li>
              Can one have the advantages of a chromatic staff and still
              differentiate between them?
            </li>
            <li>
              What kinds of symbol systems would best allow for differentiating
              between these notes on a chromatic staff, if this was desired?
            </li>
          </ul>
          <p>&nbsp;</p>
          <h2 class="divider-heading">
            The Case for Distinguishing Between Enharmonic Equivalents
          </h2>
          <p>
            The argument for visually distinguishing between enharmonically
            equivalent notes asserts that they are not completely equivalent or
            interchangeable. In this view, important musical information would
            be lost if notes like C# and Db were notated in the same way,
            collapsing the distinction between them. Twelve notes per octave are
            not enough; twenty-one notes per octave are needed, or even
            thirty-five with double sharps and flats. Whether one agrees or not,
            it is important to understand this argument, and what is at stake in
            it.
          </p>
          <p>
            There are two related reasons why distinguishing between these notes
            may be desirable:
          </p>
          <h3>1. Intonation</h3>
          <p>
            In the <a
              href="http://en.wikipedia.org/wiki/Twelve_tone_equal_temperament"
              target="_blank">twelve-tone equal temperament</a
            > tuning system, enharmonic equivalents have the same pitch. This is
            by far the most common tuning system in use in western music today, and
            has been since the romantic period of the early 1800s. In most other
            historical <a
              href="http://en.wikipedia.org/wiki/Musical_tuning#Tuning_systems"
              target="_blank">tuning systems</a
            >, which are now rarely used, and in some microtonal scales used by
            experimental musicians, these notes have slightly different pitches
            (and cease to be &#8220;enharmonically equivalent&#8221;).<a
              href="/tutorials/enharmonic-equivalents/#ftn1"
              id="ftnref1">[1]</a
            > For advanced musicians using one of these uncommon tuning systems,
            or making slight deviations in pitch for expressive purposes, a visual
            distinction between these notes is one of several factors that help them
            fine-tune their intonation. Of course, this assumes they are either singing,
            or playing a flexible-pitch instrument like a violin or trombone that
            can make these minute adjustments in pitch. Some uncommon fixed-pitch
            instruments also provide different pitches for these notes by having
            more than twelve notes per octave. For example: <a
              href="http://www.h-pi.com/eop-keyboards.html"
              target="_blank"
              >microtonal keyboards or historical keyboards with split keys</a
            >.
          </p>
          <h3>2. Harmonic and Melodic Function</h3>
          <p>
            In traditional western music theory and composition there are
            conventions about the <a
              href="http://en.wikipedia.org/wiki/Diatonic_functionality"
            >
              function</a
            > of notes based on their position within the prevailing key. These conventions
            involve using different enharmonic equivalents in order to communicate
            different types of relationships between notes. For example, in a melody
            ascending chromatically from F to G, the intermediate note is typically
            spelled as an F# rather than a Gb. The opposite would be true if the
            passage was descending chromatically from G to F. <a
              href="/tutorials/enharmonic-equivalents/#ftn2"
              id="ftnref2">[2]</a
            >  In terms of harmony, the interval between C and E is a major
            third, while the interval between C and Fb is a diminished fourth.
            Different intervals such as these convey different meanings about
            their function in a musical passage, even if they sound exactly the
            same in twelve-tone-equal-temperament (since E and Fb would have the
            same pitch). This information about the function of particular notes
            and their relation to other notes may be useful to advanced
            musicians and composers as they interpret or compose music.
          </p>
          <h3>An Example</h3>
          <div style="float: right; margin: 10px 20px 30px 50px;">
            <p>
              <img
                loading="lazy"
                alt="3 chords on a traditional staff"
                src="/wp-content/uploads/2013/03/threechords1.png"
                width="330"
                height="304"
              />
            </p>
            <p>&nbsp;</p>
            <p>
              <audio id="audio1" controls="controls"
                ><source
                  src="/audio/harmonicfunction/harmonicfunction.oga"
                  type="audio/ogg"
                /><source src="/audio/harmonicfunction/harmonicfunction.mp3" />
              </audio>
            </p>
          </div>
          <p>
            An example will help to illustrate these two different aspects. In
            the two passages on the right, the second chords (F# A# E and Gb Bb
            E) are enharmonically equivalent. They are played identically in
            both cases, if you are using equal temperament, but their harmonic
            functions are different. In the first example, the chord functions
            as a dominant seventh (V7) in the key of B, whereas in the second it
            functions as an augmented sixth triad (Aug6) in the key of Bb.<a
              href="/tutorials/enharmonic-equivalents/#ftn3"
              id="ftnref3">[3]</a
            > The spellings of these two chords are different, not just because they
            are in different keys, but also because they have different harmonic
            functions.
          </p>
          <p>
            If not playing in strict equal temperament, the two chords in
            question could also have different intonation. For example, the G
            flat in the second passage might be played slightly lower (flatter)
            than the F sharp in the first passage, to accentuate the
            &#8220;desire&#8221; of the Gb to resolve downward to F in the
            second case. That would be an example of enharmonic equivalents
            being tuned differently.
          </p>
          <p>
            However, for the same reason, the E in the second example might be
            tuned slightly higher (sharper) than the E in the first example, to
            accentuate its resolution upward to F. Notice that these two E notes
            have the same spelling in both examples (although only one requires
            a natural sign). This demonstrates that such adjustments in
            intonation are not limited to notes that are spelled differently
            (i.e., enharmonic equivalents), and often they are not directly
            indicated by traditional notation at all.
          </p>
          <p>
            In general, intonation is less a matter of following explicit cues
            given in the notation, and more a matter of playing in tune, and
            making subtle adjustments by ear, based on a note&#8217;s melodic or
            harmonic relationship to other notes. The particular spelling of a
            note is just one of several factors that might affect a note&#8217;s
            intonation. A skilled musician performing at a level where
            enharmonic equivalents are played with slightly different
            intonations will most likely be making other intonational
            adjustments that are just as significant, and will be making them by
            ear without any direct cues from traditional notation.<a
              href="/tutorials/enharmonic-equivalents/#ftn4"
              id="ftnref4">[4]</a
            >
          </p>
          <p>&nbsp;</p>
          <h2 class="divider-heading">Various Approaches</h2>
          <p>
            There are at least three different approaches to the representation
            of enharmonic equivalents in chromatic staff notation systems:
          </p>
          <ol>
            <li>
              <strong
                >Not Explicitly Differentiating Between Enharmonic Equivalents</strong
              ><br />
              &#8230;while assuming twelve-tone equal temperament for intonation
              and/or relying on contextual cues and conventions for harmonic/melodic
              function and intonation.
            </li>
            <p></p>
            <li>
              <strong>Using an Alternative Accidental Symbol System</strong><br
              />
              &#8230;to provide the same information that is given by accidental
              signs in traditional notation.
            </li>
            <p></p>
            <li>
              <strong
                >Using a More Comprehensive Microtonal Symbol System</strong
              ><br />
              &#8230;to specify intonation more precisely than is possible with standard
              accidentals,<br />
              as well as more consistently across different tuning systems and microtonal
              scales.
            </li>
          </ol>
          <p>
            These approaches involve nomenclature as well, since the traditional
            note and interval names make a distinction between enharmonic
            equivalents. For example, the first approach above lends itself to
            using a novel nomenclature for notes and intervals, otherwise the
            names of some notes and intervals would remain ambiguous.
          </p>
          <p>
            To conclude, there are different views on just how important it is
            to distinguish between enharmonic equivalents in music notation, and
            on how not doing so might affect the understanding of their
            intonation and tonal function. Fortunately, there are also
            corresponding approaches to representing them (or not) in a
            chromatic staff notation system.
          </p>
          <p>
            Intonation and tuning systems are complex topics that go beyond the
            scope of this tutorial. For further reading, see <a
              href="http://improvise.free.fr/atlas/atlas.htm"
              target="_blank">Atlas of Tonespace</a
            > from the <a href="http://improvise.free.fr/" target="_blank"
              >Intuitive Instruments for Improvisors</a
            > website. Other articles include <a
              href="http://www.emich.edu/music/wpnew/intonation.html"
              target="_blank">Intonation</a
            > by professor Julie Stone, and <a
              href="http://langemusic.com/Articles/tuning.htm"
              target="_blank">Tuning &amp; Intonation</a
            > by Joseph Butkevicius. The <a
              href="http://www.huygens-fokker.org/index_en.html"
              target="_blank">Huygens-Fokker Foundation</a
            > maintains a massive <a
              href="http://www.huygens-fokker.org/docs/bibliography.html"
              target="_blank">bibliography on tuning</a
            >, including many links to online source material.
          </p>
          <p>&nbsp;</p>
          <div class="footnotes divider-section">
            <p>
              <a href="/tutorials/enharmonic-equivalents/#ftnref1" id="ftn1"
                >[1]</a
              > Note that these various tuning systems and microtonal scales often
              give different interpretations of just how much pitch adjustment is
              indicated by traditional accidental signs. In some cases a C# has a
              higher pitch than a Db, while in other cases it is the reverse. In
              twelve-tone equal temperament enharmonic equivalents have exactly the
              same pitch because the octave is divided into twelve (logarithmically)
              equal steps, and the distance between each step is the same interval
              (a semitone). There has been a long-standing historical debate about
              the advantages and disadvantages of twelve-tone equal temperament as
              compared with other tuning systems. (For more on this see the book
              <a
                href="http://www.amazon.com/Temperament-Became-Battleground-Western-Civilization/dp/0375703306/ref=sr_1_1?ie=UTF8&s=books&qid=1232402312&sr=1-1"
                ><em>Temperament</em>
              </a> by Stuart Isacoff.)
            </p>
            <p>
              <a href="/tutorials/enharmonic-equivalents/#ftnref2" id="ftn2"
                >[2]</a
              > The logic is that in each series (F F# G and G Gb F) the final note
              serves as the resolution of the chromatic tension and is thought of
              as having its own identity, whereas the chromatic intermediate note
              is thought of as a variant of the initial note (which is less stable
              than the final note).
            </p>
            <p>
              <a href="/tutorials/enharmonic-equivalents/#ftnref3" id="ftn3"
                >[3]</a
              > &#8220;Aug6&#8221; refers to an augmented sixth chord, which contains
              the interval of an augmented sixth, Gb to E natural in this case. The
              Aug6 can be considered an altered IV chord. As in the two melodic series
              of notes in footnote two, the augmented sixth interval is an augmented
              sixth rather than a minor seventh, because the sixth (augmented or
              major, but especially augmented) &#8220;wants&#8221; to expand outwards
              to the octave, and the notes in the octave are clearly a point of repose.
              Harmony is, in some important senses, historically derivative from
              melody (via counterpoint).
            </p>
            <p>
              <a href="/tutorials/enharmonic-equivalents/#ftnref4" id="ftn4"
                >[4]</a
              > An interesting example is <a
                href="http://en.wikipedia.org/wiki/Musica_ficta">Musica Ficta</a
              >, a western musical performance practice prior to 1600 C.E. in
              which performers routinely made a full semitone&#8217;s worth of
              chromatic adjustment to certain pitches without any direct cues
              from the notation.
            </p>
          </div>
        </div>
      </article>

      <div class="divider-section">
        <nav id="nav-single">
          <h3 class="assistive-text">Tutorial navigation</h3>
          <p class="tut-nav-next">
            <a
              href="/tutorials/numerical-notation-systems/"
              rel="prev"
              class="tutorial-nav-link"
              ><span class="meta-nav">&rarr;</span> Next: Numerical Notation Systems</a
            >
          </p>
          <p class="tut-nav-previous">
            <a
              href="/tutorials/chromatic-staves-example/"
              rel="next"
              class="tutorial-nav-link"
              ><span class="meta-nav">&larr;</span> Previous: Chromatic Staves Example</a
            >
          </p>
        </nav>
      </div>
    </div>
  </div>
</MnpLayout>
