---
import MnpLayout from "../../../layouts/MnpLayout.astro";
import { makeMetaTitle } from "../../../utils/utils";
---

<MnpLayout
  metaTitle={makeMetaTitle(
    "Intervals in Traditional Music Notation",
    "Tutorials"
  )}
  metaDescription="The intervals between notes are not clear or consistent in traditional music notation. Learn how this inconsistency makes music harder to read and play."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="tutorials type-tutorials status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">Intervals in Traditional Music Notation</h1>
        </header>

        <div class="entry-content">
          <p>
            <em
              >In traditional notation the representation of intervals is
              indirect, inconsistent, and imprecise. Alternative notation
              systems can portray them with much more clarity and accuracy. This
              tutorial is part of a series that includes <a
                title="Reading and Playing Music by Intervals"
                href="/tutorials/reading-playing-music-intervals/"
                >Reading and Playing Music by Intervals</a
              >
            </em> and <a
              title="Intervals in 6-6 Music Notation Systems"
              href="/tutorials/intervals-in-6-6-music-notation-systems/"
              >Intervals in 6-6 Music Notation Systems</a
            >.
          </p>
        </div>
        <p>
          Intervals are one of the essential elements of music, the
          building-blocks that make up its structures. Melodies, scales, and
          chords are all patterns of melodic or harmonic intervals, and the
          notes in any given key belong to that key because of their interval
          relationships. Playing by ear and improvising involve hearing and
          playing music by interval relationships, and intervals are
          indispensable for understanding music theory.
        </p>
        <p>
          Because intervals are so important, the way they are represented in
          music notation is particularly significant. It is surprising how
          limited, indirect, and inconsistent the representation of intervals is
          in traditional notation. In <a
            href="/tutorials/intervals-in-6-6-music-notation-systems/"
            >the next tutorial</a
          > we explore how alternative notation systems can portray interval relationships
          with much greater clarity and accuracy. How might a better notation system
          improve awareness of interval relationships and the ability to understand
          and play music?
        </p>
        <p>
          <em>
            Note that while most of the illustrations below show harmonic
            intervals, this discussion applies equally to melodic and harmonic
            intervals.</em
          >
        </p>
        <p>&nbsp;</p>
        <h2 class="divider-heading">
          Intervals on the Traditional Diatonic Staff
        </h2>
        <p>
          At first glance traditional notation seems to represent interval
          relationships consistently. Some intervals are always one line-note
          and one space-note. Others are always two line-notes or two
          space-notes. This consistent pattern makes it easy to distinguish
          different intervals at a glance. Each interval always has its own
          particular appearance or &#8220;look&#8221; regardless of the key
          signature, clef, or accidental signs.
        </p>
        <p>
          <img
            loading="lazy"
            src="/wp-content/uploads/2013/03/notes-lines-spaces5.png"
            alt="Showing intervals by line-note and space-note patterns"
            width="624"
            height="109"
          />
        </p>
        <p>
          Despite this consistency, things are not as simple as they may appear.
          While it is easy to identify an interval&#8217;s <em>name</em> (2nd, 3rd,
          4th, etc.), it is impossible to tell from the vertical distance between
          the notes on the staff what an interval&#8217;s <em>quality</em> is (major,
          minor, perfect, augmented, diminished), or exactly how many <em
            >semitones</em
          > it spans. Many intervals that appear to be the same actually sound different
          and are played differently. For example, two thirds may look the same,
          but one may be a major third and the other a minor third.
        </p>
        <p>
          Take a moment to look at the diagram below and try to fully identify
          the intervals.
        </p>
        <ul>
          <li>
            Which 2nds, 3rds, 6ths, 7ths, and Triads are <em>major</em> and which
            are <em>minor</em>?
          </li>
          <li>
            Are the 4ths and 5ths all <em>perfect</em>, or are some <em
              >augmented</em
            > or <em>diminished</em>?
          </li>
          <li>
            How would a different clef sign or key signature change things?
          </li>
        </ul>
        <p>&nbsp;</p>
        <p>
          <strong>Diatonic Intervals in Traditional Notation</strong><br />
          Intervals that appear the same may be different, depending on staff position,
          key signature, and clef. Click the buttons below to see how different clefs
          and key signatures change the identity of the intervals, while their appearance
          on the staff remains the same.
        </p>
        <p>
          <img
            loading="lazy"
            id="traditional-intervals"
            alt="Full range of diatonic intervals in traditional notation in different clefs and key signatures"
            src="/wp-content/uploads/2012/07/traditional-intervals-default.png"
            width="700"
            height="455"
          />
        </p>
        <p>
          <a
            id="no-clef-or-key-signature-button"
            class="intervals-button cursorPointer"
            ><img
              loading="lazy"
              alt="Button for No Key Signature or Clef Sign"
              src="/wp-content/uploads/2013/03/ButtonNone2.png"
              width="80"
              height="55"
            /><br />
            No Clef or<br />
            Key Signature</a
          >
          <a
            id="treble-clef-c-major-button"
            class="intervals-button cursorPointer"
            ><img
              loading="lazy"
              alt="Button for Treble Clef, C major, A minor"
              src="/wp-content/uploads/2013/03/ButtonTreble.png"
              width="80"
              height="55"
            /><br />
            Treble Clef<br />
            C Major, A Minor</a
          >
          <a
            id="bass-clef-c-major-button"
            class="intervals-button cursorPointer"
            ><img
              loading="lazy"
              alt="Button for Bass Clef, C major, A minor"
              src="/wp-content/uploads/2013/03/ButtonBass.png"
              width="80"
              height="55"
            /><br />
            Bass Clef<br />
            C Major, A Minor</a
          >
          <a
            id="treble-clef-e-major-button"
            class="intervals-button cursorPointer"
            ><img
              loading="lazy"
              alt="Button for Treble Clef, E major, C# minor"
              src="/wp-content/uploads/2013/03/ButtonTrebleSharps.png"
              width="80"
              height="55"
            /><br />
            Treble Clef<br />
            E Major, C# Minor</a
          >
          <a
            id="bass-clef-a-flat-minor-button"
            class="intervals-button cursorPointer"
            ><img
              loading="lazy"
              alt="Button for Bass Clef, E major, C# minor"
              src="/wp-content/uploads/2013/03/ButtonBassFlats.png"
              width="80"
              height="55"
            /><br />
            Bass Clef<br />
            Ab Major, F Minor</a
          >
        </p>
        <p>
          <em
            >Numbers = semitones spanned by the interval. Maj = major. Min =
            minor. Perf = perfect. Aug = augmented. Dim = diminished.</em
          >
        </p>
        <p>
          By itself an interval&#8217;s appearance is ambiguous and does not
          provide enough information to identify the interval. To play an
          interval one must first take into account:
        </p>
        <ul>
          <li>its position on the staff</li>
          <li>the clef sign</li>
          <li>the key signature</li>
          <li>any accidental signs</li>
        </ul>
        <p>
          With all these factors in mind, one must go through the mental
          procedure of calculating the identity of the individual notes (ie:
          &#8220;C sharp and G natural&#8221;) which finally reveals their
          interval relationship. In other words, to play an interval requires
          already knowing the pitches of its notes. This affects the way in
          which music is read and played.
        </p>
        <p>
          This fundamental ambiguity and inconsistency in the appearance of
          intervals makes it much more difficult to play by reading the interval
          relationships between the notes. Because reading by intervals depends
          upon knowing the pitches of the notes, it is simpler to just play the
          notes by their pitches. This has implications for learning to
          improvise or play by ear — skills that largely entail playing by
          interval relationships rather than playing by the individual pitches
          of notes.
        </p>
        <p>
          Even in the simplest case of C major with no accidental signs,
          intervals are not clearly or directly represented in traditional
          notation. What one sees does not fully correspond with what one plays
          or hears. This frustrates and undermines a musician&#8217;s
          understanding and proficiency with the patterns of intervals found in
          diatonic music or any given style or genre of music. Consider how
          important basic intervals are obscured: whole steps and half steps
          which are the basic elements of diatonic scales and modes, and major
          and minor thirds which are the fundamental building blocks of diatonic
          chord structures.
        </p>
        <p>
          How might the aspiring musician&#8217;s understanding of music and
          proficiency with common interval patterns improve if interval
          relationships were represented clearly and were not obscured and
          hidden from view? How might the basic understanding of harmony and
          music theory improve if the appearance of interval relationships was
          clear and consistent?
        </p>
        <p>&nbsp;</p>
        <h2 class="divider-heading">Intervals and Accidental Signs</h2>
        <p>
          We have mostly been considering how the inconsistent pitch axis of the
          traditional <em>diatonic staff</em> has a distorting effect on the accurate
          representation of interval relationships. Now we will consider how <em
            >accidental signs</em
          > (and by extension <em>key signatures</em>) also distort this pitch
          axis and add another layer of complexity to reading the interval
          relationships between notes.
        </p>
        <p>
          The following illustration shows how an interval containing the same
          two staff degrees (C and D, a second) can actually have many different
          meanings in traditional notation, depending on the key signature and
          accidental signs. <a id="CD"></a>
        </p>
        <p>&nbsp;</p>
        <p>
          <strong
            >Accidental signs allow for twenty-five possible variations of any
            given interval</strong
          ><br />
          <em
            >Numbers = size of the interval in semitones. Roll your cursor over
            the image to see the more likely variations.</em
          >
          <a href="/tutorials/intervals/#ftn1" id="ftnref1">[1]</a>
        </p>
        <p>
          <a id="twenty-five-variations-image"
            ><img
              id="25variations"
              loading="lazy"
              alt="25 possible versions of a second interval from C to D with accidental signs"
              src="/wp-content/uploads/2012/07/25-variations.png"
              width="640"
              height="382"
            />
          </a>
        </p>
        <p>&nbsp;</p>
        <p>
          Although some of these twenty-five examples are largely theoretical
          and would be very improbable in actual music, there are still quite a
          few realistic cases.<a href="/tutorials/intervals/#ftn1" id="ftnref1"
            >[1]</a
          > There is a complex one-to-many mapping between (1) the vertical distance
          between two notes on the staff and (2) the many possible sizes of the interval
          that is played and heard. In other words, the visual representation is
          not proportional to the sound it is representing. What one sees does not
          correspond to what one plays or hears. In technical terms, the pitch axis
          in traditional notation is highly nonlinear.
        </p>
        <p>
          By contrast, on a chromatic staff, intervals are always unambiguous,
          being shown directly by the vertical distance between two notes, which
          is exactly proportional to the actual size of the interval as it is
          played and heard. In technical terms, the pitch axis on a chromatic
          staff is linear.<a href="/tutorials/intervals/#ftn2" id="ftnref2"
            >[2]</a
          > (Recall that on a <a href="/">chromatic staff</a> key signatures and
          accidental signs are not required.)
        </p>
        <p>&nbsp;</p>
        <p>
          <strong
            >On a chromatic staff there is just one possible meaning for any
            given interval</strong
          ><br />
          Each interval has one distinct appearance that always spans the same number
          of semitones. In this case, one semitone.
        </p>
        <p>
          <img
            loading="lazy"
            alt="an interval of a second on a chromatic staff, one semitone"
            src="/wp-content/uploads/2013/03/2ndVariationChromatic6.png"
            width="120"
            height="60"
          />
        </p>
        <p>&nbsp;</p>
        <p>
          In the tutorial on <a
            href="/tutorials/intervals-in-6-6-music-notation-systems/"
            >Intervals in 6-6 Music Notation Systems</a
          > we will explore how alternative notation systems with a chromatic staff
          improve upon traditional notation when it comes to representing interval
          relationships.
        </p>
        <p>
          But before moving on, one last illustration further demonstrates how
          the <em>appearance</em> of intervals, the vertical distance between the
          notes, may not correspond with how they <em>sound</em>, the number of
          semitones they span. (Intervals that have the same number of semitones
          are known as <a href="/tutorials/enharmonic-equivalents/"
            >enharmonic equivalents</a
          >.)
        </p>
        <p>&nbsp;</p>
        <p>
          <strong
            >In traditional notation intervals that sound the same may look
            different and vice-versa</strong
          >
        </p>
        <p>Intervals grouped by sound (semitones):</p>
        <p>
          <img
            loading="lazy"
            id="look-same-sound-different"
            alt="Full range of enharmonically equivalent intervals, grouped either by appearance or sound"
            src="/wp-content/uploads/2012/07/intervals-sound-the-same.png"
            width="725"
            height="260"
          />
        </p>
        <p>Intervals grouped by appearance (names):</p>
        <p>
          <img
            loading="lazy"
            id="look-same-sound-different"
            alt="Full range of enharmonically equivalent intervals, grouped either by appearance or sound"
            src="/wp-content/uploads/2012/07/intervals-look-the-same.png"
            width="725"
            height="260"
          />
        </p>
        <p>
          <em
            >Numbers = Semitones spanned by each interval, Maj = major, Min =
            minor, Perf = perfect, Aug = augmented, Dim = diminished, 2xA =
            doubly augmented, 2xD = doubly diminished.</em
          >
        </p>
        <p>
          How can alternative notations that use a chromatic staff and a <a
            href="/tutorials/6-6-and-7-5-pitch-patterns/">6-6 pitch pattern</a
          > improve upon these ambiguities and inconsistencies in the representation
          of interval relationships in traditional notation? Check out <a
            href="/tutorials/intervals-in-6-6-music-notation-systems/"
            >Intervals in 6-6 Music Notation Systems</a
          > to find out.
        </p>
        <p>&nbsp;</p>
        <div class="footnotes divider-section">
          <p>
            <a href="/tutorials/intervals/#ftnref1" id="ftn1">[1]</a> Rolling your
            cursor over the illustration shows eight &#8220;more likely variations.&#8221;
            These eight intervals occur in major or minor scales in keys having up
            to seven sharps or flats. More extreme intervals show up in the traditional
            repertoire &#8212; for example, Cx-Dx is found in the C# major prelude
            from Bach&#8217;s Well-Tempered Clavier book 1.
          </p>
          <p>
            <a href="/tutorials/intervals/#ftnref2" id="ftn2">[2]</a> The resolution
            of the pitch axis is generally one semitone on a chromatic staff. (A
            forthcoming tutorial will discuss microtones.) Comparing music notation
            with acoustical graphs, a linear pitch axis is equivalent to a <a
              href="http://en.wikipedia.org/wiki/Logarithm"
              target="_blank">logarithmic</a
            > frequency axis. Frequency doubles for each octave higher in pitch.
          </p>
        </div>
      </article>

      <div class="divider-section">
        <nav id="nav-single">
          <h3 class="assistive-text">Tutorial navigation</h3>
          <p class="tut-nav-next">
            <a
              href="/tutorials/reading-playing-music-intervals/"
              rel="prev"
              class="tutorial-nav-link"
              ><span class="meta-nav">&rarr;</span> Next: Reading and Playing Music
              by Intervals</a
            >
          </p>
          <p class="tut-nav-previous">
            <a
              href="/tutorials/noteheads-and-pitch/"
              rel="next"
              class="tutorial-nav-link"
              ><span class="meta-nav">&larr;</span> Previous: Noteheads and Pitch</a
            >
          </p>
        </nav>
      </div>
    </div>
  </div>
</MnpLayout>

<script>
  window.addEventListener("load", () => {
    const buttonsToHandlers = [
      [
        "no-clef-or-key-signature-button",
        "traditional-intervals",
        "/wp-content/uploads/2012/07/traditional-intervals-default.png",
      ],
      [
        "treble-clef-c-major-button",
        "traditional-intervals",
        "/wp-content/uploads/2012/07/traditional-intervals-treble.png",
      ],
      [
        "bass-clef-c-major-button",
        "traditional-intervals",
        "/wp-content/uploads/2012/07/traditional-intervals-bass.png",
      ],
      [
        "treble-clef-e-major-button",
        "traditional-intervals",
        "/wp-content/uploads/2012/07/traditional-intervals-E-major.png",
      ],
      [
        "bass-clef-a-flat-minor-button",
        "traditional-intervals",
        "/wp-content/uploads/2012/07/traditional-intervals-F-minor.png",
      ],
      [
        "twenty-five-variations-image",
        "25variations",
        "/wp-content/uploads/2012/07/25-variations-B.png",
        "mouseover",
      ],
      [
        "twenty-five-variations-image",
        "25variations",
        "/wp-content/uploads/2012/07/25-variations.png",
        "mouseout",
      ],
    ];

    buttonsToHandlers.forEach(([buttonId, imageId, newSrc, eventName]) => {
      document
        .getElementById(buttonId)
        ?.addEventListener(eventName || "click", () => {
          const image = document.getElementById(imageId) as HTMLImageElement;
          if (image?.src) {
            image.src = newSrc;
          }
        });
    });
  });
</script>
