---
import MnpLayoutWiki from "../../../../layouts/MnpLayoutWiki.astro";
import { makeMetaTitle } from "../../../../utils/utils";
---

<MnpLayoutWiki
  metaTitle={makeMetaTitle(
    "What You See is What You Play by Stuart Byrom",
    "Wiki"
  )}
  metaDescription="A wiki page about the What You See Is What You Play (WYSIWYP) music notation system by Stuart Byrom."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="wiki_page type-wiki_page status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">
            What You See Is What You Play (WYSIWYP) by Stuart Byrom
          </h1>
        </header>

        <div class="entry-content">
          <p>
            <strong>Overview</strong>
          </p>
          <p>
            <img
              loading="lazy"
              class="alignnone size-full"
              src="/wp-content/uploads/2023/03/chromatic-scale-C-to-B-sharps-combos.jpg"
              alt=""
              width="678"
              height="115"
            />
          </p>
          <p>
            Each WYSIWYP diatonic staff octave has a red line on C and a blue
            line on F. Octave numbers display to the left of the red lines.
            These lines provide a visual map to the keyboard by identifying the
            first keys in the 5-7 key groupings of each octave. Adjacent degree
            staff positions overlap 50% just like TN. The range of octaves on
            both treble and bass staves expand to fit the score (similar to the
            use of ledger lines in TN). A grey background stripe, AKA a <em
              >notetail</em
            >, indicates the note duration with respect to the beat “ticks” on
            the red and blue lines. Naturals and sharp/flat combinations (e.g.,
            C# / Db) are represented by circles and rectangles respectively.
            Each of the naturals has a unique relationship to the red and blue
            staff lines. With respect to the red line, notes B C D are touching
            it from below, over, and above. The same relationships exist with E
            F G and the blue line. Not touching any line is A. The flat/sharp
            combination noteheads are displayed in the overlap space between
            their adjacent naturals' staff positions. The notetail of a sharp
            has the rectangular notehead at the top of the stripe, while that of
            a flat has the notehead at the bottom. The figure above has sharp
            notetails and the following has flat notetails. The noteheads
            themselves do not move.
          </p>
          <p>
            <img
              loading="lazy"
              class="alignnone size-full"
              src="/wp-content/uploads/2023/03/chromatic-scale-C-to-B-flats-combos.jpg"
              alt=""
              width="670"
              height="115"
            />
          </p>
          <p>
            Here is a two-octave example with all the sharps stacked up on the
            left (just to emphasize the visual mapping) and all of the naturals
            spread out on the right:
          </p>
          <p>
            <img
              loading="lazy"
              class="alignnone size-full"
              src="/wp-content/uploads/2023/03/WYSIWYP-keyboard-mapping.jpg"
              alt=""
              width="624"
              height="317"
            />
          </p>
          <p>Here's an example score snippet.</p>
          <p>
            <img
              loading="lazy"
              class="alignnone size-full"
              src="/wp-content/uploads/2023/03/1ere-Gymnopedie-without-measure-numbers.jpg"
              alt=""
              width="720"
              height="290"
            />
          </p>
          <p>
            The design elements described above are the WYSIWYP changes to TN.
            All other TN symbols are to remain intact in the WYSIWYP design. The
            design goal for WYSIWYP is to be logical and visually intuitive
            while solving the three major problems of Traditional Notation:
            inconsistent staves, the need for key signatures, and nonintuitive
            rhythm. To achieve this goal, a WYSIWYP octave is consistent
            regardless of staff and its position on a staff. Noteheads
            <em>explicitly</em> define naturals, sharps, and flats so there is no
            need for key signatures to <em>implicitly</em> define them. Note durations
            are visually intuitive on a timeline of beats and there is no need for
            a time signature.
          </p>
          <p>
            <strong>Simplified Notation app (SNapp)</strong>
          </p>
          <p>
            An essential element of any notation is the availability of sheet
            music, either printed or digital. After about 100 years,
            Klavarskribo has produced a large inventory of printed sheet music
            available for sale (thanks to a dedicated and wealthy designer and
            sponsor). However, with modern electronic device apps (and the
            absence of wealthy sponsors), there is no need to recreate a printed
            inventory for a new notation. Accepting input files in MusicXML
            format (the current prevalent standard for digital music scores),
            apps can convert and display virtual sheet music on a screen device
            (smart phone, pad, PC). WYSIWYP's Simplified Notation app, SNapp, is
            an internet browser-based app designed to run in any browser on any
            operating system. Thus, input MusicXML scores can be displayed on
            most any device display. It can also produce a PDF file from which
            the score may be printed. In addition to providing sheet music
            display, apps can also provide a wide variety of user preference
            options for customizing the display format. SNapp allows the user to
            set the number of measures per sheet music row, page layout
            controls, lyrics size, notehead size, etc. In addition, it also
            makes possible the ability to customize the notation itself. For
            example, while the default notehead symbols are circles and
            rectangles, different symbols can be selected such as separate sharp
            and flat notehead symbols.
          </p>
          <p>
            <img
              loading="lazy"
              class="alignnone size-full"
              src="/wp-content/uploads/2023/03/chromatic-scale-C-to-B-sharps-triangles.jpg"
              alt=""
              width="678"
              height="115"
            />
          </p>
          <p>
            <img
              loading="lazy"
              class="alignnone size-full"
              src="/wp-content/uploads/2023/03/chromatic-scale-C-to-B-flats-triangles.jpg"
              alt=""
              width="670"
              height="116"
            />
          </p>
          <p>
            As with TN, recognizing intervals between notes in a chord is
            difficult due to the inconsistent intervals between adjacent
            diatonic naturals across the scale. Thus chords that have the same
            appearance can have different intervals (and vice-versa). However,
            with SNapp's user preference for color-coded chords, intervals
            within a chord are easily identified by color (and thus the chord
            itself can be identified by its unique interval sequence of colors).
            For example, green represents an interval of four and yellow an
            interval of three. These C Major scale triads have the same "look"
            (stacked circles) but have three different interval (color)
            sequences (43, 34, 33):
          </p>
          <p>
            <img
              loading="lazy"
              class="alignnone"
              src="/wp-content/uploads/2023/03/C-Major-triads.jpg"
              alt=""
              width="325"
              height="211"
            />
          </p>
          <p>
            These twelve Major triads look different but all have the same
            interval (color) sequence (43):
          </p>
          <p>
            <img
              loading="lazy"
              class="alignnone"
              src="/wp-content/uploads/2023/03/Color-coded-Major-triads.jpg"
              alt=""
              width="695"
              height="160"
            />
          </p>
          <p>
            User preferences actually blur a strict definition of a notation and
            allow users to have their own customized sheet music. Even the same
            user may want to choose different preferences according to the
            score, the genre, or whether the user is learning a new score or
            performing a known one. In this way, the WYSIWYP notation is further
            expanded by SNapp to make it a customizable and personal notation
            for the user. At this time, SNapp is a prototype and does not
            include all of the unchanged functionality of TN. It does support
            all of the changed functionality described herein and should be
            suitable for design evaluation as well as beginning students of
            music. SNapp is open-source and can be found on the hosting site
            GitHub.
          </p>
          <p>
            <strong>Advantages of WYSIWYP</strong>
          </p>
          <p>
            The WYSIWYP diatonic design requires the same vertical space as TN
            wherein the seven natural noteheads overlap by 50% on the staff.
            Only two staff lines per octave are needed to unambiguously define
            the seven degree staff positions, thereby avoiding numerous and
            confusing staff lines. Explicit noteheads for naturals, sharps, and
            flats replace the need for key signatures. Notation for rhythm is
            visually intuitive. One of the simplicities of the WYSIWYP octave is
            that the staff lines represent the start of the 5-7 key groups on
            the keyboard and thus provide a direct visual mapping from the
            notation. Unlike chromatic staff designs, WYSIWYP focuses on the
            seven naturals supporting cognitive psychology research that says
            that the number of objects an average human can hold in short-term
            memory is seven plus or minus two (see “Weblinks” list below). All
            of these simplifying factors may be helpful to beginning students of
            music. Explicit noteheads for naturals, sharps, and flats plus the
            staff line mapping to the keyboard make WYSIWYP a true keyboard
            tablature and therefore intuitive to read and visually mapped to the
            keyboard. By reducing the mental gymnastics from sheet music to
            fingers, the notation is dubbed What You See Is What You Play. Some
            students may find the challenge of reading TN and learning to play
            at the same time is too great and just give up playing an
            instrument. WYSIWYP may help these students gain some experience
            playing before taking on TN. Later, transitioning from WYSIWYP to TN
            may be less difficult than from other alternative notations because
            WYSIWYP retains some of the basic conceptual elements of TN. These
            include the diatonic scale staff, a 50% overlap of adjacent staff
            degrees, a horizontal timeline, noteheads that define note onset,
            and a requirement for real-time playing adjustments for
            sharps/flats.
          </p>
          <p>
            <strong>WYSIWYP Project Mission</strong>
          </p>
          <p>
            One mission of WYSIWYP notation is to make reading sheet music as
            simple and intuitive as possible while still providing all of the
            functionality of Traditional Notation (TN), thus making it suitable
            for musicians of all levels of experience. Beginning students of the
            piano may get the most benefit due to the notation serving as a
            keyboard tablature. This simplified notation is intended to reduce
            the barrier of reading sheet music in the hopes of reducing student
            drop-out rates and to support lifelong playing of music. This would
            be a boon to music schools, private instructors, musical instrument
            manufacturers, makers of music instruction materials, and musicians
            everywhere. But having a good design does not guarantee it will be
            discovered and adopted in the mainstream world of music. Therefore,
            another mission of the WYSIWYP project is to provide the tools to
            enable this. These tools include the development of an informational
            website, the availability of sheet music (via MusicXML and SNapp),
            and instructional materials. To date the WYSIWYP project has a
            prototype website that enables the notation to be found by internet
            search engines. The website introduces the notation and explains how
            to use the WYSIWYP display app, SNapp. It also includes an example
            of a piano course for beginners and provides a few sample scores
            available for download.
          </p>
          <p>
            <strong>Conclusion</strong>
          </p>
          <p>
            It must be said that there is not a “one size fits all” solution for
            notation. Advanced musicians, composers, and musicologists may
            prefer the more analytic format of chromatic designs. At the
            opposite end of the skill spectrum are beginners who may prefer more
            graphic and visually intuitive designs and tablatures like
            Klavarskribo and WYSIWYP (note that the former has thousands of
            lifelong users). There is no (logical) reason why a player should
            not be able to select a notation that suits the score, the genre,
            and the player's skill level. And WYSIWYP may be a good option for
            beginning piano students who are struggling to read sheet music.
            Obviously, there is a long way to go from prototypes to full
            function website and app capable of attracting and holding a user
            community. And convincing traditionalists that there may be a better
            way remains a formidable challenge. “A long journey begins with the
            first step.” &nbsp;
          </p>
          <p>
            <strong>Weblinks</strong>
          </p>
          <p>
            WYSIWYP prototype website:
            <a href="https://www.wysiwyp.org/">https://www.wysiwyp.org/</a>
          </p>
          <p>
            SNapp screen device prototype app:
            <a href="https://snapp.wysiwyp.org/">https://snapp.wysiwyp.org/</a>
          </p>
          <p>
            The Magic Number Seven, Plus or Minus 2:
            <a
              href="https://en.wikipedia.org/wiki/The_Magical_Number_Seven,_Plus_or_Minus_Two"
              >https://en.wikipedia.org/wiki/The_Magical_Number_Seven,_Plus_or_Minus_Two</a
            >
          </p>
          <p>
            The problems with Traditional Notation:
            <a href="https://musicnotation.org/">https://musicnotation.org/</a>
          </p>
          <p>
            Concerns with Traditional Notation Rhythm Notation:
            <a
              href="https://musicnotation.org/wiki/rhythm-notation/concerns-traditional-rhythm-notation/"
              >https://musicnotation.org/wiki/rhythm-notation/concerns-traditional-rhythm-notation/</a
            >
          </p>
          <p>
            Problems reading Intervals in Traditional Music Notation:
            <a href="https://musicnotation.org/tutorials/intervals/"
              >https://musicnotation.org/tutorials/intervals/</a
            >
          </p>
          <p>
            Reading and Playing Music by Intervals:
            <a
              href="https://musicnotation.org/tutorials/reading-playing-music-intervals/"
              >https://musicnotation.org/tutorials/reading-playing-music-intervals/</a
            >
          </p>
          <p>
            WYSIWYP Color-coded chords:
            <a href="https://groups.google.com/g/musicnotation/c/2vbjTIKw5t0"
              >https://groups.google.com/g/musicnotation/c/2vbjTIKw5t0</a
            >
          </p>
          <p>
            WYSIWYP Chromatic Look on a Diatonic Staff (see attachment to forum
            post):
            <a href="https://groups.google.com/g/musicnotation/c/tS-q3jFtVBo"
              >https://groups.google.com/g/musicnotation/c/tS-q3jFtVBo</a
            >
          </p>
          <p>
            The MuseScore website has a wide range of scores available for
            download; scores in the public domain are free while others require
            a membership subscription:
            <a href="https://musescore.com/sheetmusic"
              >https://musescore.com/sheetmusic</a
            >
          </p>
          <p>&nbsp;</p>
        </div>
      </article>
    </div>
  </div>
</MnpLayoutWiki>
