---
import MnpLayoutWiki from "../../../../layouts/MnpLayoutWiki.astro";
import { makeMetaTitle } from "../../../../utils/utils";
---

<MnpLayoutWiki
  metaTitle={makeMetaTitle("TwinNote by Paul Morris", "Wiki")}
  metaDescription="Documentation of TwinNote music notation system, an alternative music notation system by Paul Morris that uses triangle shaped note heads and a chromatic staff."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="wiki_page type-wiki_page status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">TwinNote by Paul Morris</h1>
        </header>

        <div class="entry-content">
          <p>
            TwinNote Music Notation by Paul Morris was designed in December of
            2009 when it was first introduced on this wiki page. For more
            complete documentation of TwinNote, see the <a
              href="http://clairnote.org/twinnote/"
              rel="nofollow">TwinNote Music Notation</a
            > website and also <a
              href="/system/twinnote-by-paul-morris/"
              rel="nofollow">this page</a
            > on the Music Notation Project&#8217;s website.
          </p>
          <p>
            TwinNote was inspired and influenced by <a
              title="Twinline Notation by Thomas Reed"
              href="/system/twinline-notation-by-thomas-reed/"
              rel="nofollow">Twinline notation</a
            > in its various versions by de Vries, Reed, and Keislar (as well as
            <a
              href="/system/bilinear-notation-by-jose-a-sotorrio/"
              rel="nofollow">Bilinear notation</a
            > by Sotorrio). When compared to <a
              href="/system/black-oval-twinline-by-paul-morris/"
              rel="nofollow">Black-Oval Twinline</a
            > (Morris&#8217; version of Twinline and previous notation system of
            choice), TwinNote is simpler, using two note shapes, triangles pointing
            either up or down. The two orientations of the triangles highlight the
            <a href="/tutorials/6-6-and-7-5-pitch-patterns/" rel="nofollow"
              >6-6 pitch pattern</a
            >. In one whole tone scale all the triangles point upwards, and in
            the other they all point downwards. In a chromatic scale there is a
            regularly alternating pattern of triangles pointing upward and
            downward. This way of representing a 6-6 pitch pattern gives
            intervals a more clear and consistent appearance (than is found in
            Black-Oval Twinline notation, for example).
          </p>
          <p>
            Below is a chromatic scale with alternating solid and hollow
            noteheads.  This was the original note layout, but it is no longer
            accurate.  In March 2011 Morris shifted the notes up a semitone so
            that the notes C D E F G A B would correspond to the lines and
            spaces of the traditional treble clef staff. The images below show
            the previous note assignments (with only C D E corresponding with
            the treble staff).
          </p>
          <p>
            <img
              loading="lazy"
              alt="TriTwNt-BigChromaticScale2B.png"
              src="/wp-content/uploads/2013/03/500px-TriTwNt-BigChromaticScale2B.png"
              width="500"
              height="163"
            />
          </p>
          <p>
            <em>
              Chromatic scale with original note layout (no longer accurate).</em
            >
          </p>
          <p>&nbsp;</p>
          <h2>Interval Profile</h2>
          <p>
            Below is an llustration showing TwinNote&#8217;s complete
            &#8220;interval profile.&#8221; You can see that the intervals in
            this system have a more consistent appearance compared to <a
              href="/wiki/notation-systems/black-oval-twinline-by-paul-morris/"
              >intervals in Black-Oval Twinline</a
            >. (See also this discussion of <a
              href="http://clairnote.org/twinnote/learn/influences/"
              rel="nofollow">influences on TwinNote</a
            >.)
          </p>
          <p>
            <img
              loading="lazy"
              alt="TwNt Intervals from PDF.png"
              src="/wp-content/uploads/2013/03/800px-TwNt_Intervals_from_PDF.png"
              width="800"
              height="1262"
            />
          </p>
          <p>&nbsp;</p>
          <h2>Pitch-Proportionality</h2>
          <p>
            Below is an illustration showing how TwinNote is
            &#8220;pitch-proportional,&#8221; having a proportional vertical
            pitch axis:
          </p>
          <p>
            <img
              loading="lazy"
              alt="TriangleTwNt-pitchprop-equilateral.png"
              src="/wp-content/uploads/2013/03/TriangleTwNt-pitchprop-equilateral.png"
              width="1000"
              height="300"
            />
          </p>
          <p>
            The distance between the centers of any two noteheads is always
            proportional to the musical interval between them. See the
            MNP&#8217;s <a href="/systems/criteria/#8" rel="nofollow"
              >8th criterion</a
            > for alternative notation systems: &#8220;The notation possesses a fully
            proportional pitch coordinate, where each of the twelve common pitches
            is spaced in a graphic manner, so that progressively larger pitch intervals
            have progressively larger spacing on the coordinate, providing a visual
            representation of each interval that is exactly proportional to its actual
            sound.&#8221;
          </p>
          <p>
            In the image above the centers of the triangles are shown at 3/8 of
            the triangle&#8217;s height (the distance from its base to its tip).
            In geometry there are various kinds of centers for any given
            triangle, for example: <a
              href="http://www.mathopenref.com/triangleincenter.html"
              rel="nofollow">Incenter</a
            >, <a
              href="http://www.mathopenref.com/trianglecircumcenter.html"
              rel="nofollow">Circumcenter</a
            >, <a
              href="http://www.mathopenref.com/trianglecentroid.html"
              rel="nofollow">Centroid</a
            >, and <a
              href="http://www.mathopenref.com/triangleorthocenter.html"
              rel="nofollow">Orthocenter</a
            >. The most relevant center in this case is arguably the centroid,
            also known as the triangle&#8217;s &#8220;center of gravity&#8221;.
          </p>
          <p>
            For an isosceles triangle like the ones used in TwinNote, the
            centroid is located at 1/3 (0.333&#8230;) of the triangle&#8217;s
            height. (Interestingly enough, changing the width of an isosceles
            triangle does not affect the centroid&#8217;s position on the
            vertical axis.) 1/3 of the height is only slightly less than the 3/8
            (0.375) shown in the image above.
          </p>
          <p>
            To find the difference: 0.375 minus 0.333&#8230; equals
            0.041666&#8230; of the height of the triangle. The triangle&#8217;s
            height is the same as the distance between two staff lines which are
            commonly spaced about 1/16 (0.0625) of an inch apart. So 0.0416
            times 0.0625 is only 0.0026 of an inch in terms of actual real-world
            difference between the centroid and the 3/8 point.
          </p>
          <p>
            This is so miniscule that it is effectively negligible, and could be
            easily negated by small adjustments in musical fontography and
            engraving. Consider also that the notes as perceived on the page are
            not pure, ideal, abstract geometric objects, and are subject to the
            vicissitudes of human perception. (How does the line thickness of
            hollow notes affect the perception of their center? How does the
            thickness of the staff lines come into play? Is the base of the
            triangle the top or bottom edge of the staff line it rests on?  How
            important are the positions of the top and bottom edges/points of
            two triangles relative to each other, as compared with their
            centers, when determining relative vertical position?)
          </p>
          <p>
            The following image illustrates the difference between the centroid
            at 1/3 and the 3/8 point used in the image above. Even at this large
            magnification, the difference is quite small.
          </p>
          <p>
            <img
              loading="lazy"
              alt="TwinNote-Centroid-Example.png"
              src="/wp-content/uploads/2013/03/TwinNote-Centroid-Example.png"
              width="613"
              height="291"
            />
          </p>
        </div>
      </article>
    </div>
  </div>
</MnpLayoutWiki>
