---
import MnpLayoutWiki from "../../../../layouts/MnpLayoutWiki.astro";
import { makeMetaTitle } from "../../../../utils/utils";
---

<MnpLayoutWiki
  metaTitle={makeMetaTitle("Chromatic Pairs by Fernando Terra", "Wiki")}
  metaDescription="Documentation and discussion of Chromatic Pairs by Fernando Terra, an alternative notation system that uses a 6-degree-per-octave staff similar to Equiton."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="wiki_page type-wiki_page status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">Chromatic Pairs by Fernando Terra</h1>
        </header>

        <div class="entry-content">
          <p>
            Chromatic Pairs is an alternative music notation system by Fernando
            Terra introduced in October of 2011.
          </p>
          <h2>Introduction</h2>
          <p>
            Chromatic Pairs is based on the basic five-line chromatic staff.
            Below is an image of the chromatic scale on a traditional diatonic
            staff and a basic five-line chromatic staff. (This is just one of <a
              href="/systems/gallery/"
              rel="nofollow">many different versions</a
            > of chromatic staff.)
          </p>
          <p>&nbsp;</p>
          <div>
            <a href="/wiki/File:Im_1_Chromatic_scale_on_traditional.png"
              ><img
                loading="lazy"
                alt="Im 1 Chromatic scale on traditional.png"
                src="/wp-content/uploads/2013/03/diatonicTreble577.png"
                width="577"
                height="100"
              />
            </a>
          </div>
          <p>&nbsp;</p>
          <div>
            <a href="/wiki/File:Im_2_Chromatic_scale_on_Basic_Chromatic.png"
              ><img
                loading="lazy"
                alt="Im 2 Chromatic scale on Basic Chromatic.png"
                src="/wp-content/uploads/2013/03/chromatic5line577.png"
                width="577"
                height="114"
              />
            </a>
          </div>
          <p>&nbsp;</p>
          <p>
            This basic chromatic staff has great advantages over current
            traditional notation, as described on the Music Notation
            Project&#8217;s <a href="" rel="nofollow">home page</a>. However,
            the octave spans the full staff (one octave occupies six lines and
            spaces on the staff), which may make it inadequate if the music has
            a wide range.
          </p>
          <p>
            The basic objective of Chromatic Pairs, therefore, was to retain as
            much of the simplicity of this basic chromatic staff as possible,
            while allowing more notes to be represented in the same space.
          </p>
          <p>
            Chromatic Pairs represents two notes on each line and two notes on
            each space of the staff, doubling the amount of notes represented in
            the basic chromatic.
          </p>
          <p>
            A two octave chromatic progression in Chromatic Pairs notation is
            shown below:<br />
            <img
              loading="lazy"
              alt="Page11 CP bakedi CDE.jpg"
              src="/wp-content/uploads/2013/03/Page11_CP_bakedi_CDE.jpg"
              width="1181"
              height="313"
            /><br />
            Notice that, as with a basic chromatic staff, in Chromatic Pairs there
            is also no need for key signatures, clefs or accidentals.
          </p>
          <p>&nbsp;</p>
          <h2>Staff, Note Names, and Intervals</h2>
          <p>
            Before any further explanation on the notation I would like to
            suggest that in addition to using a different staff notation system
            you would consider using a different nomenclature for the notes and
            different interval naming and notation, because they all share the
            same characteristic which ultimately makes understanding music much
            more difficult. This characteristic is that they all have an
            implicit musical format.
          </p>
          <p>
            In this sense, the traditional staff notation and note names are
            worse than the intervals, since they are bound to a very specific
            format, the C major/A minor key. In this case, even if the music is
            in a common major or minor key, if it is not in C major/A minor, the
            music will be poorly represented and unnecessarily difficult. The
            interval notation should not be as bad, because it is not bound to a
            specific key, but it is based on the diatonic format, therefore
            being a bad option if the music departs from that format.
            Additionally, the interval notation does not profit from using the
            half-step as a unit, which might be the worst problem of the
            interval notation (more on this below).
          </p>
          <p>
            The traditional approach does not take full advantage of equal
            temperament. Twelve tone equal temperament is the most common tuning
            today. In this system, the octave is divided equally in twelve
            half-steps.(1) If you then take the half-step as a unit, you can
            apply the properties of numbers (like relative position and
            counting), which are logical and familiar, to notes. This, however,
            is not easily done using traditional musical tools (the traditional
            staff, note names and intervals).
          </p>
          <p>
            That said, we can now come back to the Chromatic Pairs notation that
            is shown below:
          </p>
          <p>
            <img
              loading="lazy"
              alt="Page11 CP bakedi CDE.jpg"
              src="/wp-content/uploads/2013/03/Page11_CP_bakedi_CDE.jpg"
              width="1181"
              height="313"
            />
          </p>
          <p>
            In the picture you see a chromatic progression of pitches starting
            in &#8216;Ba&#8217;. &#8216;Ba&#8217; is equal to C in the ‘Bakedi’
            note naming system, which I suggest as a better alternative to the
            alphabetic naming (although one can use the Chromatic Pairs notation
            with traditional note naming).
          </p>
          <p>&nbsp;</p>
          <h2>Bakedi Nomenclature</h2>
          <p>
            Bakedi is a simple nomenclature. I believe most people would be able
            to memorize it in less than five minutes.
          </p>
          <p>Here is how Bakedi equates to the alphabetical system.</p>
          <table>
            <tbody>
              <tr>
                <th>Ba</th>
                <th>Be</th>
                <th>Bi</th>
                <th>Bo</th>
                <th>Ka</th>
                <th>Ke</th>
                <th>Ki</th>
                <th>Ko</th>
                <th>Da</th>
                <th>De</th>
                <th>Di</th>
                <th>Do</th>
              </tr>
              <tr>
                <td>C</td>
                <td>C#</td>
                <td>D</td>
                <td>D#</td>
                <td>E</td>
                <td>F</td>
                <td>F#</td>
                <td>G</td>
                <td>G#</td>
                <td>A</td>
                <td>A#</td>
                <td>B</td>
              </tr>
            </tbody>
          </table>
          <p>
            In addition to being simple, this nomenclature takes full advantage
            of equal temperament. The purpose of Bakedi is basically to allow
            FAST INTERVAL CALCULATION and NOTE POSITION establishment. This is
            possible by means of treating the half-step as a unit and,
            therefore, profiting from the characteristics of numbers.
          </p>
          <p>In fact, the names were conceived to be NUMBERS.</p>
          <p>
            If I may give an example, imagine a sequence of thirty numbers
            starting in zero
          </p>
          <table>
            <tbody>
              <tr>
                <td>0</td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>5</td>
                <td>6</td>
                <td>7</td>
                <td>8</td>
                <td>9</td>
              </tr>
              <tr>
                <td>10</td>
                <td>11</td>
                <td>12</td>
                <td>13</td>
                <td>14</td>
                <td>15</td>
                <td>16</td>
                <td>17</td>
                <td>18</td>
                <td>19</td>
              </tr>
              <tr>
                <td>20</td>
                <td>21</td>
                <td>22</td>
                <td>23</td>
                <td>24</td>
                <td>25</td>
                <td>26</td>
                <td>27</td>
                <td>28</td>
                <td>29</td>
              </tr>
            </tbody>
          </table>
          <p>
            Now imagine that we don&#8217;t use the decimal system, which is
            based on groups of ten numbers, but a system based on groups of four
            numbers, as such
          </p>
          <table>
            <tbody>
              <tr>
                <td>0</td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
              </tr>
              <tr>
                <td>10</td>
                <td>11</td>
                <td>12</td>
                <td>13</td>
              </tr>
              <tr>
                <td>20</td>
                <td>21</td>
                <td>22</td>
                <td>23</td>
              </tr>
            </tbody>
          </table>
          <p>
            Bakedi uses the letters &#8216;aeio&#8217; to be the names and
            symbols of the &#8220;numbers&#8221; &#8216;0123&#8217;
          </p>
          <table>
            <tbody>
              <tr>
                <th>a</th>
                <th>e</th>
                <th>i</th>
                <th>o</th>
              </tr>
              <tr>
                <td>0</td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
              </tr>
            </tbody>
          </table>
          <p>
            and consonants &#8216;B K D&#8217; to correspond respectively to 0,
            1 and 2, as the &#8220;tens&#8221; of the numbers. This is
            illustrated in the table below;
          </p>
          <table>
            <tbody>
              <tr>
                <td></td>
                <td></td>
                <td>0</td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td>a</td>
                <td>e</td>
                <td>i</td>
                <td>o</td>
              </tr>
              <tr>
                <td>0</td>
                <td>B</td>
                <td>Ba</td>
                <td>Be</td>
                <td>Bi</td>
                <td>Bo</td>
              </tr>
              <tr>
                <td>1</td>
                <td>K</td>
                <td>Ka</td>
                <td>Ke</td>
                <td>Ki</td>
                <td>Ko</td>
              </tr>
              <tr>
                <td>2</td>
                <td>D</td>
                <td>Da</td>
                <td>De</td>
                <td>Di</td>
                <td>Do</td>
              </tr>
            </tbody>
          </table>
          <p>
            In this way I have 12 notes Ba Be Bi Bo Ka Ke Ki Ko Da De Di Do,
            which are divided equally in three sets of four notes.
          </p>
          <p>
            The sequence of vowels (aeio) is popularly known, and known in that
            specific order (from a to o), since children are educated that way
            while learning to read. I believe this note naming gives a good idea
            to the musician of the notes positions in relation to each other,
            and that could allow for the musician not to depart as much from the
            notes in ABSOLUTE terms (the actual note names), when they need to
            think in RELATIVE terms (i.e. intervals).
          </p>
          <p>&nbsp;</p>
          <h2>Intervals</h2>
          <p>
            Regarding intervals I just use the names of the numbers, i.e. if a
            note is four half-steps apart, then their interval is four. Here is
            how this system equates to the traditional system:
          </p>
          <table>
            <tbody>
              <tr>
                <th>0</th>
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
                <th>6</th>
                <th>7</th>
                <th>8</th>
                <th>9</th>
                <th>10</th>
                <th>11</th>
              </tr>
              <tr>
                <td>unison</td>
                <td>Minor 2nd</td>
                <td>Major 2nd</td>
                <td>Minor 3rd</td>
                <td>Major 3rd</td>
                <td>Perfect 4th</td>
                <td>Dim 5th</td>
                <td>Perfect 5th</td>
                <td>Minor 6th</td>
                <td>Major 6th</td>
                <td>Minor 7th</td>
                <td>Major 7th</td>
              </tr>
            </tbody>
          </table>
          <p>
            Beyond the octave a possibility is to use the letter &#8211; o
            &#8211; for octave, and proceed as o+1, o+2, o+3, … or o1, o2, o3, …
          </p>
          <p>&nbsp;</p>
          <h2>Chromatic Pairs Features</h2>
          <p>
            I would now like to talk about some of the characteristics of the
            CHROMATIC PAIRS notation. Here is the Chromatic Pairs notation
            again:
          </p>
          <p>
            <img
              loading="lazy"
              alt="Page11 CP bakedi CDE.jpg"
              src="/wp-content/uploads/2013/03/Page11_CP_bakedi_CDE.jpg"
              width="1181"
              height="313"
            />
          </p>
          <p>
            In Chromatic Pairs a chromatic progression occurs as successive
            changes in color and upward jumps as shown above.
          </p>
          <p>
            Although this is a bit more complex than the basic chromatic staff
            (shown below),
          </p>
          <p>
            <img
              loading="lazy"
              alt="Im 2 Chromatic scale on Basic Chromatic.png"
              src="/wp-content/uploads/2013/03/chromatic5line577.png"
              width="577"
              height="114"
            />
          </p>
          <p>image source: musicnotation.org</p>
          <p>
            Chromatic Pairs is favored by some nice properties. It is organized
            in PAIRS of BLACK and WHITE and counting in pairs (0,2,4,6,8,10) is
            almost as easy as counting units. It is also organized in GROUPS OF
            FOUR HALF-STEPS from one line to the next, as explained below:
          </p>
          <p>In every staff there are four types of notes:</p>
          <ul>
            <li>Black notes on lines (notes ending in a, Ba Ka Da)</li>
            <li>White notes on lines (notes ending in e, Be Ke De)</li>
            <li>Black notes on spaces (notes ending in i, Bi Ki Di)</li>
            <li>White notes on spaces (notes ending in o, Bo Ko Do)</li>
          </ul>
          <p>
            (You can notice how these four types of notes relate conveniently to
            Bakedi.)
          </p>
          <p>
            Therefore, the interval of four half-steps is very easily
            recognizable. To jump to a note four half-steps apart just go to the
            next note of the same kind. By doing that, you will also arrive in a
            note ending in the same vowel in Bakedi.
          </p>
          <p>
            It is essential for a notation to be logical, so you can follow some
            simple steps to conclude what is the interval between two given
            notes, while learning the notation. However, the idea is that, with
            some practice, you could abandon those steps and just instantly
            recognize any interval.
          </p>
          <h3>Relating Chromatic Pairs to Bakedi</h3>
          <p>
            As said earlier for each type of note representation in the staff,
            there is a vowel.
          </p>
          <ul>
            <li>Black notes on lines (notes ending in a, Ba Ka Da)</li>
            <li>White notes on lines (notes ending in e, Be Ke De)</li>
            <li>Black notes on spaces (notes ending in i, Bi Ki Di)</li>
            <li>White notes on spaces (notes ending in o, Bo Ko Do)</li>
          </ul>
          <p>
            You can also notice every BLACK NOTE ends either with
            &#8216;A&#8217; or &#8216;I&#8217;, every WHITE NOTE ends either
            with &#8216;E&#8217; or &#8216;O&#8217;.
          </p>
          <p>
            Besides that, every consonant represents a line and a space (the
            space immediately above it) on each octave, as shown in the image
            below, and this also represents a PROGRESSION IN 4 HALF-STEP
            INTERVALS.
          </p>
          <p>
            <img
              loading="lazy"
              alt="Page12 CP bakedi consonants.jpg"
              src="/wp-content/uploads/2013/03/Page12_CP_bakedi_consonants.jpg"
              width="1063"
              height="579"
            />
          </p>
          <p>&nbsp;</p>
          <h2>The Octaves Display</h2>
          <p>
            To the LEFT OF THE STAFF you can see numbers that show the OCTAVES
            which encompasses the notes shown at their right in the staff.
          </p>
          <p>
            <img
              loading="lazy"
              alt="Page13 CP octave numbers.jpg"
              src="/wp-content/uploads/2013/03/Page13_CP_octave_numbers.jpg"
              width="1181"
              height="325"
            />
          </p>
          <p>
            So, for instance &#8216;Ba4&#8217; to &#8216;Do4&#8217; have to
            their left the number four, above &#8216;Do4&#8217; the next note is
            &#8216;Ba5&#8217; and this is represented in the left as the
            beginning of the fifth octave.
          </p>
          <p>
            In the ledger line region there are two octaves that use the same
            space (as in traditional notation). From bottom to top, in our
            example above, the fourth octave is represented by a 4 with an arrow
            upwards coming from the third octave. From top to bottom there is a
            similar indication of the third octave coming from the fourth.
          </p>
          <p>
            These octaves numbers are assignable and should be determined
            according to the musician’s necessity.
          </p>
          <p>
            <img
              loading="lazy"
              alt="Page14 CP different octave settings.jpg"
              src="/wp-content/uploads/2013/03/Page14_CP_different_octave_settings.jpg"
              width="1122"
              height="549"
            />
          </p>
          <h2>Rhythm</h2>
          <p>
            Chromatic Pairs borrows the rhythm of the traditional notation
            almost entirely, but since there is a conflict with the half-note
            and quarter-note because of the use of color to determine pitch, the
            half-note is changed to the form shown in the image below:
          </p>
          <p>
            <img
              loading="lazy"
              alt="Page15 CP half-note.jpg"
              src="/wp-content/uploads/2013/03/Page15_CP_half-note.jpg"
              width="337"
              height="224"
            />
          </p>
          <p>
            An explanation on the rhythm of notations that use color to indicate
            pitch is given in <a href="/tutorials/noteheads-and-pitch/"
              >this tutorial</a
            >.
          </p>
          <p>&nbsp;</p>
          <h2>Conclusion</h2>
          <p>
            The traditional musical staff notation and traditional nomenclature,
            both of intervals and notes, is overly complex and do not represent
            the music well. This is due to the fact that they have an intrinsic
            musical format.
          </p>
          <p>
            This problem seems worst for the note nomenclature and traditional
            staff, since they are bound to a very specific format, the C major/A
            minor key. This bond inflicts a lot of unnecessary effort on the
            musician, who has work around this format.
          </p>
          <p>
            Additionally, the traditional system doesn’t benefit enough from the
            properties of numbers, which can be used when you consider the
            half-step to be a unit.
          </p>
          <p>
            Chromatic Pairs combined with the Bakedi nomenclature and different
            names for intervals were proposed as alternatives to the traditional
            system. This new system has the objective to be as simple as
            possible and to allow for the use of the half-step as unit,
            therefore permitting fast note positioning establishing and interval
            calculation.
          </p>
          <p>
            The ultimate purpose of the system is to make learning, reading,
            writing and understanding music easier.
          </p>
          <hr />
          <p>
            Note on Equiton: Although Chromatic Pairs was conceived based on the
            basic chromatic notation alone and without previous knowledge of
            similar notations, there is a notation which is similar to Chromatic
            Pairs in regard to pitch, but that makes further modifications to
            the traditional notation. The name of this notation is Equiton, and
            it was designed by Rodney Fawcett in 1958. In <a
              href="/wiki/notation-systems/equiton-music-notation-by-rodney-fawcett/"
              >this page</a
            > you will find a description of Rodney Fawcett’s notation along with
            a link to a page that describes his work (although it is a very bad translation),
            and a pdf file of Mark Gould’s work, that further develops on Equiton.
          </p>
          <p>
            (1) This is a crude definition of twelve tone equal temperament, but
            fits the purpose of a quick explanation. On the following link the
            term is better defined <a
              href="http://en.wikipedia.org/wiki/Equal_temperament"
              rel="nofollow">http://en.wikipedia.org/wiki/Equal_temperament</a
            >
          </p>
        </div>
      </article>
    </div>
  </div>
</MnpLayoutWiki>
