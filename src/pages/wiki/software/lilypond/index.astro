---
import MnpLayoutWiki from "../../../../layouts/MnpLayoutWiki.astro";
import { makeMetaTitle } from "../../../../utils/utils";
---

<MnpLayoutWiki
  metaTitle={makeMetaTitle("LilyPond", "Wiki")}
  metaDescription="A wiki page about LilyPond as software that can create sheet music in alternative music notation systems."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="wiki_page type-wiki_page status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">LilyPond</h1>
        </header>

        <div class="entry-content">
          <p>
            <a href="http://lilypond.org" target="_blank">Lilypond</a> is free, open-source
            music notation software that can be used to create sheet music in alternative
            notation systems. One of LilyPond&#8217;s strengths is its flexibility. 
            It is designed so that its output can be modified using the <a
              href="https://en.wikipedia.org/wiki/Scheme_%28programming_language%29"
              target="_blank">Scheme</a
            > programming language.  This opens the door to a world of customization
            without having to alter LilyPond&#8217;s source code. This page documents
            how to use LilyPond with alternative music notation systems with a chromatic
            staff.
          </p>
          <h2>Files</h2>
          <p>
            The following files can be used to produce music in a number of
            alternative music notation systems.  These files demonstrate the use
            of the specific &#8220;lower-level&#8221; techniques documented
            below.
          </p>
          <ul>
            <li>
              <a
                title="MNP-scripts.ly"
                href="/wp-content/uploads/2015/01/MNP-scripts.ly"
              >
                MNP-scripts.ly
              </a> — A LilyPond file containing LilyPond and Scheme code that supports
              alternative music notation systems.
            </li>
            <li>
              <a
                title="MNP-music-demo.ly"
                href="/wp-content/uploads/2015/01/MNP-music-demo.ly"
              >
                MNP-music-demo.ly
              </a> — A LilyPond music file demonstrating the use of the <code
                >MNP-scripts.ly</code
              > file.
            </li>
            <li>
              <a
                title="MNP-music-demo.pdf"
                href="/wp-content/uploads/2015/01/MNP-music-demo.pdf"
              >
                MNP-music-demo.pdf
              </a> — LilyPond&#8217;s PDF output from the <code
                >MNP-music-demo.ly</code
              > file.
            </li>
          </ul>
          <p>
            There are basically just a few steps to use an existing traditional
            LilyPond music file to create a PDF of music in an alternative
            notation system:
          </p>
          <ol>
            <li>
              Download the <code>MNP-scripts.ly</code> file above and put it in the
              same directory as your music file.
            </li>
            <li>
              Include the <code>MNP-scripts.ly</code> file at the top of the music
              file. (See <a
                href="http://lilypond.org/doc/v2.18/Documentation/notation/including-lilypond-files.html"
                rel="nofollow">Including LilyPond Files</a
              >.) The <code>MNP-scripts.ly</code> file and the music file must be
              in the same directory for the <code>\include</code> command to work.
            </li>
            <li>
              Change &#8220;Staff&#8221; in the music file to the name of any
              alternative staff that is defined at the bottom of the <code
                >MNP-scripts.ly</code
              > file. (Examples are &#8220;StaffFiveLineOne&#8221; or &#8220;StaffKlavarMirckTwo&#8221;.
              See the <code>MNP-music-demo.ly</code> file for more.)
            </li>
            <li>
              If the alternative staff is larger than two octaves and is not a
              compressed staff (i.e. Twinline, TwinNote), then you may need to
              make the following adjustment for any bass clefs that appear in
              the music. Enter &#8220;<code
                >\set Staff.middleCPosition = #18</code
              >&#8221; just after any instances of &#8220;<code>\clef bass</code
              >&#8221; in the music file. Otherwise the notes may not appear in
              the expected places on the staff.
            </li>
            <li>
              Run LilyPond on your music file. Voilà! You now have a PDF of
              music in an alternative notation system.
            </li>
          </ol>
          <p><strong>Older Demo Files</strong></p>
          <p>
            These are some older demo files that are inferior to those shown
            above.
          </p>
          <ul>
            <li>
              Chromatic staves demo, showing various line patterns and note
              layouts — <a
                title="Chromatic-staves-demo.pdf"
                href="/wp-content/uploads/2013/03/Chromatic-staves-demo.pdf"
              >
                PDF file</a
              > — <a
                title="Chromatic-staves-demo.ly"
                href="/wp-content/uploads/2013/03/Chromatic-staves-demo.ly"
              >
                LilyPond file (.ly)</a
              >
            </li>
            <li>
              Custom note head styles (shapes) demo — <a
                title="Custom-notehead-styles-demo.pdf"
                href="/wp-content/uploads/2013/03/Custom-notehead-styles-demo.pdf"
              >
                PDF file</a
              > — <a
                title="Custom-notehead-styles-demo.ly"
                href="/wp-content/uploads/2013/03/Custom-notehead-styles-demo.ly"
              >
                LilyPond file (.ly)</a
              >
            </li>
            <li>
              Twinline music notation demo — <a
                title="Twinline-demo.pdf"
                href="/wp-content/uploads/2013/03/Twinline-demo.pdf"
              >
                PDF file</a
              > — <a
                title="Twinline-demo.ly"
                href="/wp-content/uploads/2013/03/Twinline-demo.ly"
              >
                LilyPond file (.ly)</a
              >
            </li>
            <li>
              TwinNote music notation demo — <a
                title="TwinNote-demo.pdf"
                href="/wp-content/uploads/2013/03/TwinNote-demo.pdf"
              >
                PDF file</a
              > — <a
                title="TwinNote-demo.ly"
                href="/wp-content/uploads/2013/03/TwinNote-demo.ly"
              >
                LilyPond file (.ly)</a
              >
            </li>
          </ul>
          <h2>Customizing Note Positions</h2>
          <p><strong>Semitone Spacing</strong></p>
          <p>
            <code>staffLineLayoutFunction</code> is used to customize the default
            vertical positions of notes on the staff. For a standard pitch-proportional
            chromatic staff with a semitone between each adjacent note position (line
            or space), set it to use the <code>ly:pitch-semitones</code> function:
          </p>
          <p><code>staffLineLayoutFunction = #ly:pitch-semitones</code></p>
          <ul>
            <li>
              <a href="http://lsr.dsi.unimi.it/LSR/Item?id=694" rel="nofollow"
                >Changing the interval of lines on the stave
                (staffLineLayoutFunction)</a
              >
            </li>
            <li>
              <a
                href="http://git.savannah.gnu.org/gitweb/?p=lilypond.git;a=blob;f=input/regression/chromatic-scales.ly"
                rel="nofollow">Example use of staffLineLayoutFunction</a
              >
            </li>
            <li>
              <a
                href="http://www.mail-archive.com/lilypond-user@gnu.org/msg55671.html"
                rel="nofollow">Email exchange about this example</a
              >
            </li>
            <li>
              <a
                href="http://www.lilypond.org/doc/v2.18/Documentation/notation/scheme-functions"
                rel="nofollow">Scheme Functions</a
              > ( <code>ly:pitch-semitones</code> is a Scheme function )
            </li>
          </ul>
          <p><strong>Other Spacing Patterns</strong></p>
          <p>
            For notation systems like <a
              href="/wiki/notation-systems/equiton-music-notation-by-rodney-fawcett/"
            >
              Equiton</a
            > or <a href="http://clairnote.org/twinnote/" rel="nofollow"
              >TwinNote</a
            > that have a staff based on a whole tone scale with a whole tone between
            each adjacent note position (line or space), use the following function.
            (Thanks to Graham Breed for this function.)
          </p>
          <p>
            <code
              >staffLineLayoutFunction = #(lambda (p) (floor (/ (+
              (ly:pitch-semitones p) 0) 2)))</code
            >
          </p>
          <p>
            For staves based on other intervals simply change the number 2 in
            the function above to the number of semitones in the interval you
            want. Change the number 0 in the formula to 1 (or 2, etc.) to adjust
            which notes share a line or space (and which are on different lines
            or spaces).  See: <a
              href="http://lsr.dsi.unimi.it/LSR/Item?u=1&id=755"
              rel="nofollow"
              >Snippet: Staves based on a whole tone scale (or other interval)</a
            >
          </p>
          <p>
            For <a
              href="/system/twinline-notation-by-thomas-reed/"
              rel="nofollow">Twinline</a
            > and similar chromatic staves with one note on a line and three in a
            space, use the following version of the function which uses &#8220;banker&#8217;s
            rounding&#8221; to get the desired 1-3-1-3-1-3&#8230; pattern.
          </p>
          <p>
            <code
              >staffLineLayoutFunction = #(lambda (p) (+ 1 (round (+ -1 (/
              (ly:pitch-semitones p) 2)))))</code
            >
          </p>
          <p>
            (Note: Graham Breed has also shown how to use the <code
              >ly:set-default-scale</code
            > scheme function to reposition notes: <a
              href="http://lsr.dsi.unimi.it/LSR/Item?id=619"
              rel="nofollow">Dodecaphonic staff snippet</a
            >)
          </p>
          <p>&nbsp;</p>
          <h2>Customizing Staff Lines</h2>
          <p>
            In LilyPond the vertical position of notes and the vertical position
            of staff lines are independent of each other.  Notes are not placed
            on a line or space per se, but at a vertical position that may or
            may not coincide with the vertical position of a line or space.
          </p>
          <p>
            A staff&#8217;s line pattern can be customized using <code
              >Staff.StaffSymbol</code
            > and the <code>line-positions</code> property. See <a
              href="http://www.lilypond.org/doc/v2.18/Documentation/notation/staff-symbol-properties"
              target="_blank"
              rel="nofollow"
              >Changing the staff line pattern (Staff Symbol Properties)</a
            >. The numbers ( 4 2 0 -2 -4 ) in the line of code below represent
            the five standard line positions.  A different list of numbers will
            produce a custom line pattern.
          </p>
          <p>
            <code
              >\override Staff.StaffSymbol #'line-positions = #'( 4 2 0 -2 -4 )</code
            >
          </p>
          <p>
            It is possible to simulate bold lines by adding extra lines close
            together with the <code>line-positions</code> property.  Go to <a
              href="http://www.lilypond.org/doc/v2.18/Documentation/notation/modifying-single-staves"
              target="_blank"
              rel="nofollow">Modifying a single staff</a
            > and scroll down to &#8220;Making some staff lines thicker than the
            others&#8221; for more about this trick.
          </p>
          <p>
            Dashed or dotted lines can be achieved using the Scheme function
            found in <a
              href="http://lists.gnu.org/archive/html/lilypond-user/2012-12/msg00733.html"
              rel="nofollow">this post</a
            > to the LilyPond user list.  This function is also included in the <code
              >MNP-scripts.ly</code
            > file linked at the top of this page.
          </p>
          <p>&nbsp;</p>
          <h2>Customizing Ledger Lines</h2>
          <p>
            In LilyPond version 2.15.13 or higher, ledger line positions can be
            customized, including ledger lines that appear above or below the
            staff and those that are internal to it. Similar to customizing
            staff lines, you use <code>Staff.StaffSymbol</code> with the <code
              >ledger-positions</code
            > property:
          </p>
          <p>
            <code
              >\override Staff.StaffSymbol #'ledger-positions = #'(-12 -10 -8 -6
              -4 -2 (0 2) 4 6 8 10 12)</code
            >
          </p>
          <p>
            The numbers define positions for a series of ledger lines, this
            series is repeated in a cycle extending both up and down the pitch
            axis. The last number in the series is also the first ledger line in
            the next repetition of the cycle (i.e. the last number refers to the
            same ledger line as the first number, just in the next iteration of
            the series).
          </p>
          <p>
            Internal ledger lines appear by default when custom staff line
            positions are set with a gap between them of 4 steps or more.
          </p>
          <p>
            You can group multiple ledger lines together by placing them in
            parentheses, as shown above: (0 2). When one of the ledger lines in
            a group appears all of the others will appear as well. This can be
            used to achieve thicker, bold ledger lines by using the same trick
            described above for bold staff lines.
          </p>
          <p>
            <i
              >Thanks goes to Piers Titus for his work adding this feature to
              LilyPond. (This supersedes <a
                href="/wiki/software-wiki/kevin-dalleys-lilypond-code/"
                >Kevin Dalley&#8217;s code</a
              > for internal ledger lines that was never added to LilyPond.) <a
                href="http://code.google.com/p/lilypond/issues/detail?id=1193#makechanges"
                target="_blank"
                rel="nofollow">More details&#8230;</a
              >
            </i>
          </p>
          <p><strong>An Older Deprecated Method</strong></p>
          <p>
            Before the method above was available, internal ledger lines could
            be achieved by creating a custom notehead stencil (glyph) that has
            the ledger line included in it, and then assigning that notehead
            stencil to the pitches that require a ledger line. This is how it is
            done in the TwinNote Demo file (linked above). With systems that do
            not already use custom note heads this would be more difficult since
            you would have to access to the default oval note head stencil and
            combine it with a ledger line stencil. You would also need to create
            different stencils for quarter notes, half notes, and whole notes,
            and then have a script to determine when to use which based on a
            note&#8217;s duration.
          </p>
          <p>&nbsp;</p>
          <h2>Customizing Noteheads</h2>
          <p>
            Noteheads can be colored: <a
              href="http://lsr.dsi.unimi.it/LSR/Item?id=572"
              rel="nofollow">Coloring notes depending on their pitch</a
            >
          </p>
          <p>
            Notehead styles (shapes) can be customized using a modified version
            of the code for coloring note heads. See our <a
              title="Custom-notehead-styles-demo.ly"
              href="/wp-content/uploads/2013/03/Custom-notehead-styles-demo.ly"
            >
              Custom-notehead-styles-demo.ly</a
            > for the modified code.  This offers more fine-grained control than
            LilyPond&#8217;s <code
              ><a
                href="http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Note-heads#Shape-note-heads"
                rel="nofollow">shapeNoteStyles</a
              >
            </code> property that is used for Shape Note notation. That property
            only allows you to set custom shapes for each step of the scale (as set
            by the key signature or the tonic property). It does not let you customize
            each chromatic scale degree. For example, whatever shape you designate
            for G will also be the shape for G# and Gb.)
          </p>
          <p>Documentation for notehead styles (shapes):</p>
          <ul>
            <li>
              <a
                href="http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Note-head-styles"
                rel="nofollow">Note Head Styles</a
              >
            </li>
            <li>
              <a
                href="http://lilypond.org/doc/v2.12/Documentation/user/lilypond/The-Feta-font#Special-Notehead-glyphs"
                rel="nofollow">Feta Font: Special Notehead Glyphs</a
              >
            </li>
            <li>
              <a
                href="http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Note-heads#Note-heads"
                rel="nofollow">Note Heads (in general)</a
              >
            </li>
          </ul>
          <p>
            Custom notehead shapes can be created with <code
              >ly:make-stencil</code
            >, see this snippet on <a
              href="http://lsr.dsi.unimi.it/LSR/Item?id=516"
              rel="nofollow"
              >Using PostScript to generate special note head shapes</a
            >. This PostScript stencil method was used to create the triangle
            note shapes in these TwinNote demo files:
          </p>
          <ul>
            <li>
              <a
                title="TwinNote-demo.ly"
                href="/wp-content/uploads/2013/03/TwinNote-demo.ly"
                >TwinNote Demo LilyPond file (.ly)</a
              >
            </li>
            <li>
              <a
                title="TwinNote-demo.pdf"
                href="/wp-content/uploads/2013/03/TwinNote-demo.pdf"
                >TwinNote Demo PDF file</a
              >
            </li>
          </ul>
          <p>
            This TwinNote demo also has a different, more streamlined script for
            customizing noteheads based on pitch.
          </p>
          <p>&nbsp;</p>
          <h2>Customizing Stems</h2>
          <p>
            This override prevents LilyPond from extending stems to the middle
            of the staff. This is particularly helpful with multiple-octave
            chromatic staves, otherwise you can get stems that extend an octave
            or more. It is shown in this snippet: <a
              href="http://lsr.dsi.unimi.it/LSR/Item?id=141"
              rel="nofollow">Preventing Stem Extension</a
            >
          </p>
          <p><code>\override Stem #'no-stem-extend = ##t</code></p>
          <p>
            The following TwinNote demo LilyPond file (.ly) includes a script
            for adjusting the stems so that they &#8220;attach&#8221; to the
            note head correctly in TwinNote (not too short or too long). It also
            includes a script that gives half notes a double stem to
            differentiate them from quarter notes.
          </p>
          <ul>
            <li>
              <a
                title="TwinNote-demo.ly"
                href="/wp-content/uploads/2013/03/TwinNote-demo.ly"
                >TwinNote Demo LilyPond file (.ly)</a
              >
            </li>
            <li>
              <a
                title="TwinNote-demo.pdf"
                href="/wp-content/uploads/2013/03/TwinNote-demo.pdf"
                >TwinNote Demo PDF file</a
              >
            </li>
          </ul>
          <p>&nbsp;</p>
          <h2>Removing Unneeded Symbols</h2>
          <p>
            Accidental signs, key signatures, and clefs can be removed as
            follows. Other unneeded symbols can be hidden: <a
              href="http://www.lilypond.org/doc/v2.18/Documentation/notation/visibility-of-objects"
              rel="nofollow">Making symbols invisible</a
            >
          </p>
          <p>
            <code
              >\remove "Accidental_engraver"<br />
              \remove "Key_engraver"<br />
              \remove "Clef_engraver"</code
            >
          </p>
          <h2>Desired Features List</h2>
          <p>
            Most desired features can now be achieved as described above. Any
            additional desired features can be listed here, should they come to
            light.
          </p>
        </div>
      </article>
    </div>
  </div>
</MnpLayoutWiki>
